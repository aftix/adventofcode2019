#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <qlibc/qlibc.h>

#include "timing.h"

struct orbit {
  struct orbit * suborbits[64];
  size_t numorbits;
  char name[4];
};

static size_t
getcount(struct orbit * o, size_t depth)
{
  if (!o) {
    return 0;
  }

  size_t inter = 0;
  ++depth;

  for (size_t i=0; i < o->numorbits; ++i) {
    inter += getcount(o->suborbits[i], depth);
  }

  if (depth > 1) {
    inter += depth - 1;
  }

  return inter;
}

int
main(int argc, char ** argv)
{
  uint64_t start = ns();

  qtreetbl_t * allorbs = qtreetbl(0);

  struct orbit porbit;
  struct orbit corbit;
  struct orbit * pptr = NULL;
  struct orbit * cptr = NULL;

  struct orbit ** optr = NULL;

  for (size_t i=0; i < sizeof(corbit.suborbits) / sizeof(corbit.suborbits[0]); ++i) {
    porbit.suborbits[i] = NULL;
    corbit.suborbits[i] = NULL;
  }
  porbit.numorbits = 0;
  corbit.numorbits = 0;

  int ch = 0;
  size_t ppos = 0;
  size_t cpos = 0;

  for (ch = getchar(); ch != EOF; ch = getchar()) {
    while (ch != (int)')') {
      porbit.name[ppos++] = (char)ch;
      ch = getchar();
    }
    porbit.name[ppos] = '\0';
    ppos = 0;

    ch = getchar();
    while (ch != (int)'\n' && ch != EOF) {
      corbit.name[cpos++] = (char)ch;
      ch = getchar();
    }
    corbit.name[cpos] = '\0';
    cpos = 0;

    optr = allorbs->get(allorbs, porbit.name, NULL, false);
    if (!optr) {
      pptr = malloc(sizeof(porbit));
      memcpy(pptr, &porbit, sizeof(porbit));
      allorbs->put(allorbs, porbit.name, &pptr, sizeof(pptr));
    } else {
      pptr = *optr;
    }


    optr = allorbs->get(allorbs, corbit.name, NULL, false);
    if (!optr) {
      cptr = malloc(sizeof(corbit));
      memcpy(cptr, &corbit, sizeof(corbit));
      allorbs->put(allorbs, corbit.name, &cptr, sizeof(cptr));
    } else {
      cptr = *optr;
    }

    pptr->suborbits[pptr->numorbits] = cptr;
    pptr->numorbits += 1;
  }

  pptr = *((struct orbit **)allorbs->get(allorbs, "COM", NULL, false));

  printf("%lu\n", getcount(pptr, 0));

  showtiming(start);
  return 0;
}
