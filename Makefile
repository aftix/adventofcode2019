srcs=$(shell find . -maxdepth 1 -name "*.c" -type f | grep -v "intcode.c")
targets=$(patsubst %.c,exe/%,$(srcs))

all: exe exe/intcode.o $(targets)

exe:
	@mkdir exe

exe/intcode.o: intcode.c intcode.h
	clang -c -march=native -O3 -ffast-math $< -o $@

exe/%: %.c exe/intcode.o
	clang -march=native -O3 -ffast-math $< exe/intcode.o -o $@ -lm -lpthread -lqlibc
