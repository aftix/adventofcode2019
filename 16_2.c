#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <stdint.h>

#include <pthread.h>

#include "timing.h"

#define NUMTHREAD 4

size_t offset = 0;
size_t vlen = 0;

struct pdata {
  size_t beg, end;
  bool even;
  int * vec;
  int * temp;
};

void
iteration(int * out, const int * in, size_t begin, size_t end)
{
}

void *
runthread(void * data)
{
  struct pdata * d = (struct pdata *)data;
  int * in = d->vec;
  int * out = d->temp;
  if (!d->even) {
    in = d->temp;
    out = d->vec;
  }

  int sum = 0;
  for (size_t i=d->beg; i < vlen; ++i) {
    sum += in[i];
  }

  out[d->beg] = abs(sum) % 10;

  for (size_t i=d->beg + 1; i < d->end; ++i) {
    sum -= in[i - 1];
    out[i] = abs(sum) % 10;
  }

  return NULL;
}

int
main(int argc, char ** argv)
{
  uint64_t start = ns();

  char * input = NULL;
  size_t inlen = 0;

  FILE * buf = open_memstream(&input, &inlen);

  for (int inc = getchar(); inc != EOF && inc != '\n'; inc = getchar()) {
    fputc((char)inc - '0', buf);
  }

  fclose(buf);

  offset = (size_t)input[0];
  for (size_t i=1; i < 7; ++i) {
    offset *= 10;
    offset += (size_t)input[i];
  }

  vlen = inlen * 10000 - offset - 1;

  int * vec = malloc(sizeof(int) * vlen);

  size_t ipos = offset % inlen;
  for (size_t i=0; i < vlen; ++i) {
    vec[i] = input[ipos++];
    if (ipos == inlen) {
      ipos = 0;
    }
  }

  int * temp = malloc(sizeof(int) * vlen);

  struct pdata threaddata[NUMTHREAD];

  const size_t perthread = vlen / NUMTHREAD;
  size_t thisbeg = 0;
  size_t thisend = perthread;
  for (size_t i=0; i < NUMTHREAD - 1; ++i) {
    threaddata[i].even = true;
    threaddata[i].vec = vec;
    threaddata[i].temp = temp;
    threaddata[i].beg = thisbeg;
    threaddata[i].end = thisend;

    thisbeg += perthread;
    thisend += perthread;
  }

  threaddata[NUMTHREAD - 1].even = true;
  threaddata[NUMTHREAD - 1].vec = vec;
  threaddata[NUMTHREAD - 1].temp = temp;
  threaddata[NUMTHREAD - 1].beg = thisbeg;
  threaddata[NUMTHREAD - 1].end = vlen;

  pthread_t threads[NUMTHREAD];

  for (size_t i=0; i < 100; ++i) {
    for (size_t j=0; j < NUMTHREAD; ++j) {
      pthread_create(&threads[j], NULL, runthread, &threaddata[j]);
    }
    for (size_t j=0; j < NUMTHREAD; ++j) {
      pthread_join(threads[j], NULL);
      threaddata[j].even ^= true;
    }
  }

  for (size_t i=0; i < 8; ++i) {
    putc((char)vec[i] + '0', stdout);
  }
  putc('\n', stdout);

  showtiming(start);
}
