#include <stdio.h>
#include <stdlib.h>

#include "timing.h"

int
main(int argc, char ** argv)
{
  uint64_t start = ns();

  int inc = EOF;
  int sum = 0;
  int in = 0;

  for (inc = getchar(); inc != EOF; inc = getchar()) {
    if (inc == (int)'\n') {
      sum += (in / 3 - 2);
      in = 0;
      continue;
    }

    if (in) {
      in *= 10;
    }

    in += (inc - (int)'0');
  }

  if (in) {
    sum += (in / 3 - 2);
  }

  printf("%d\n", sum);

  showtiming(start);
}
