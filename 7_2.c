#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <unistd.h>

#include <pthread.h>

#include "timing.h"
#include "perms_2.h"

int tape[1000];

void
simulate(int * tape, int fin, int fout)
{
  size_t tpos = 0;
  int ch = 0;
  int num = 0;

  while (tape[tpos] != 99) {
    switch (tape[tpos]) {
    // Opcode 1: ADD
    case 1:
      tape[tape[tpos + 3]] = tape[tape[tpos + 1]] + tape[tape[tpos + 2]];
      tpos += 4;
      break;
    case 101:
      tape[tape[tpos + 3]] = tape[tpos + 1] + tape[tape[tpos + 2]];
      tpos += 4;
      break;
    case 1001:
      tape[tape[tpos + 3]] = tape[tape[tpos + 1]] + tape[tpos + 2];
      tpos += 4;
      break;
    case 1101:
      tape[tape[tpos + 3]] = tape[tpos + 1] + tape[tpos + 2];
      tpos += 4;
      break;

    //Opcode 2: MULT
    case 2:
      tape[tape[tpos + 3]] = tape[tape[tpos + 1]] * tape[tape[tpos + 2]];
      tpos += 4;
      break;
    case 102:
      tape[tape[tpos + 3]] = tape[tpos + 1] * tape[tape[tpos + 2]];
      tpos += 4;
      break;
    case 1002:
      tape[tape[tpos + 3]] = tape[tape[tpos + 1]] * tape[tpos + 2];
      tpos += 4;
      break;
    case 1102:
      tape[tape[tpos + 3]] = tape[tpos + 1] * tape[tpos + 2];
      tpos += 4;
      break;

    //Opcode 3: RDI
    case 3:
      /*ch = getchar();
      if (ch == EOF) {
        fprintf(stderr, "Need an input\n");
        return 1;
      } else if (ch == (int)'-') {
        ch = -1;
        num = (getchar() - (int)'0');
      } else if (ch == (int)'+') {
        ch = 1;
        num = (getchar() - (int)'0');
      } else {
        num = (ch - (int)'0');
        ch = 1;
      }

      for (int c = getchar(); c != EOF && c != '\n'; c = getchar()) {
        num *= 10;
        num += (c - (int)'0');
      }
      tape[tape[tpos + 1]] = num * ch;*/
      read(fin, &tape[tape[tpos + 1]], sizeof(tape[0]));
      tpos += 2;
      break;

    //Opcode 4: PRI
    case 4:
      //printf("%d\n", tape[tape[tpos + 1]]);
      write(fout, &tape[tape[tpos + 1]], sizeof(tape[0]));
      tpos += 2;
      break;
    case 104:
      // printf("%d\n", tape[tpos + 1]);
      write(fout, &tape[tpos + 1], sizeof(tape[0]));
      tpos += 2;
      break;

    //Opcode 5: JNZ
    case 5:
      if (tape[tape[tpos + 1]]) {
        tpos = tape[tape[tpos + 2]];
      } else {
        tpos += 3;
      }
      break;
    case 105:
      if (tape[tpos + 1]) {
        tpos = tape[tape[tpos + 2]];
      } else {
        tpos += 3;
      }
      break;
    case 1005:
      if (tape[tape[tpos + 1]]) {
        tpos = tape[tpos + 2];
      } else {
        tpos += 3;
      }
      break;
    case 1105:
      if (tape[tpos + 1]) {
        tpos = tape[tpos + 2];
      } else {
        tpos += 3;
      }
      break;

    // Opcode 6: JZ
    case 6:
      if (!tape[tape[tpos + 1]]) {
        tpos = tape[tape[tpos + 2]];
      } else {
        tpos += 3;
      }
      break;
    case 106:
      if (!tape[tpos + 1]) {
        tpos = tape[tape[tpos + 2]];
      } else {
        tpos += 3;
      }
      break;
    case 1006:
      if (!tape[tape[tpos + 1]]) {
        tpos = tape[tpos + 2];
      } else {
        tpos += 3;
      }
      break;
    case 1106:
      if (!tape[tpos + 1]) {
        tpos = tape[tpos + 2];
      } else {
        tpos += 3;
      }
      break;

    // Opcode 7: LT
    case 7:
      if (tape[tape[tpos + 1]] < tape[tape[tpos + 2]]) {
        tape[tape[tpos + 3]] = 1;
      } else {
        tape[tape[tpos + 3]] = 0;
      }
      tpos += 4;
      break;
    case 107:
      if (tape[tpos + 1] < tape[tape[tpos + 2]]) {
        tape[tape[tpos + 3]] = 1;
      } else {
        tape[tape[tpos + 3]] = 0;
      }
      tpos += 4;
      break;
    case 1007:
      if (tape[tape[tpos + 1]] < tape[tpos + 2]) {
        tape[tape[tpos + 3]] = 1;
      } else {
        tape[tape[tpos + 3]] = 0;
      }
      tpos += 4;
      break;
    case 1107:
      if (tape[tpos + 1] < tape[tpos + 2]) {
        tape[tape[tpos + 3]] = 1;
      } else {
        tape[tape[tpos + 3]] = 0;
      }
      tpos += 4;
      break;

    // Opcode 8: EQ
    case 8:
      if (tape[tape[tpos + 1]] == tape[tape[tpos + 2]]) {
        tape[tape[tpos + 3]] = 1;
      } else {
        tape[tape[tpos + 3]] = 0;
      }
      tpos += 4;
      break;
    case 108:
      if (tape[tpos + 1] == tape[tape[tpos + 2]]) {
        tape[tape[tpos + 3]] = 1;
      } else {
        tape[tape[tpos + 3]] = 0;
      }
      tpos += 4;
      break;
    case 1008:
      if (tape[tape[tpos + 1]] == tape[tpos + 2]) {
        tape[tape[tpos + 3]] = 1;
      } else {
        tape[tape[tpos + 3]] = 0;
      }
      tpos += 4;
      break;
    case 1108:
      if (tape[tpos + 1] == tape[tpos + 2]) {
        tape[tape[tpos + 3]] = 1;
      } else {
        tape[tape[tpos + 3]] = 0;
      }
      tpos += 4;
      break;

    // No opcode
    default:
      fprintf(stderr, "Invalid opcode %d\n", tape[tpos]);
      exit(1);
    }
  }
}

struct simdata {
  int * tape;
  int fin;
  int fout;
};

void *
runsim(void * data)
{
  struct simdata * sd = data;
  simulate(sd->tape, sd->fin, sd->fout);
  return NULL;
}

int
main(int argc, char ** argv)
{
  uint64_t start = ns();
  memset(tape, 0, sizeof(tape));

  size_t tpos = 0;
  int inc = 0;
  int sign = 1;

  inc = getchar();
  if (inc == (int)'-') {
    sign = -1;
    tape[0] = (getchar() - (int)'0');
  } else if (inc == (int)'+') {
    tape[0] = (getchar() - (int)'0');
  } else {
    tape[0] = (inc - (int)'0');
  }

  for (inc = getchar(); inc != EOF && inc != '\n'; inc = getchar()) {
    if (inc == (int)',') {
      if (sign < 0) {
        tape[tpos] = -tape[tpos];
      }
      inc = getchar();
      if (inc == (int)'-') {
        sign = -1;
        tape[++tpos] = (getchar() - (int)'0');
      } else if (inc == (int)'+') {
        sign = 1;
        tape[++tpos] = (getchar() - (int)'0');
      } else {
        sign = 1;
        tape[++tpos] = (inc - (int)'0');
      }
      continue;
    }

    tape[tpos] *= 10;
    tape[tpos] += (inc - (int)'0');
  }

  int amptapes[5][sizeof(tape) / sizeof(tape[0])];
  int amppipes[5][2];

  pipe(amppipes[0]);
  pipe(amppipes[1]);
  pipe(amppipes[2]);
  pipe(amppipes[3]);
  pipe(amppipes[4]);

  const int startin = 0;

  pthread_t threads[5];

  struct simdata senddata[5];
  for (size_t i=0; i < sizeof(senddata) / sizeof(senddata[0]); ++i) {
    senddata[i].tape = amptapes[i];
    senddata[i].fin = amppipes[i][0];
    senddata[i].fout = amppipes[(i + 1) % 5][1];
  }

  int maxout = INT_MIN;
  int output = 0;
  for (size_t try = 0; try < sizeof(perms) / sizeof(perms[0]); ++try) {
    memcpy(amptapes[0], tape, sizeof(tape));
    memcpy(amptapes[1], tape, sizeof(tape));
    memcpy(amptapes[2], tape, sizeof(tape));
    memcpy(amptapes[3], tape, sizeof(tape));
    memcpy(amptapes[4], tape, sizeof(tape));

    write(amppipes[0][1], &perms[try][0], sizeof(perms[try][0]));
    write(amppipes[1][1], &perms[try][1], sizeof(perms[try][0]));
    write(amppipes[2][1], &perms[try][2], sizeof(perms[try][0]));
    write(amppipes[3][1], &perms[try][3], sizeof(perms[try][0]));
    write(amppipes[4][1], &perms[try][4], sizeof(perms[try][0]));
    write(amppipes[0][1], &startin, sizeof(startin));

    for (size_t i=0; i < 5; ++i) {
      pthread_create(&threads[i], NULL, runsim, &senddata[i]);
    }

    for (size_t i=0; i < 5; ++i) {
      pthread_join(threads[i], NULL);
    }

    read(amppipes[0][0], &output, sizeof(output));
    if (output > maxout) {
      maxout = output;
    }
  }

  printf("%d\n", maxout);

  showtiming(start);

  return 0;
}
