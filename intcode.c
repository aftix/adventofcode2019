#include <stdio.h>
#include <unistd.h>

#include "intcode.h"

long maintape[TAPELEN];

void
intcode_run(long * tape, int fin, int fout, bool bin, bool bout)
{
  struct intcode_state state = (struct intcode_state) {
    .tape = tape,
    .fin = fin,
    .fout = fout,
    .tpos = 0,
    .offset = 0,
    .bufferin = bin,
    .bufferout = bout
  };

  char buffer[32];

  while (state.tape[state.tpos] != 99) {
    switch (state.tape[state.tpos]) {
    // Opcode 1: ADD
    case 1:
      state.tape[state.tape[state.tpos + 3]] = state.tape[state.tape[state.tpos + 1]] + state.tape[state.tape[state.tpos + 2]];
      state.tpos += 4;
      break;
    case 101:
      state.tape[state.tape[state.tpos + 3]] = state.tape[state.tpos + 1] + state.tape[state.tape[state.tpos + 2]];
      state.tpos += 4;
      break;
    case 201:
      state.tape[state.tape[state.tpos + 3]] = state.tape[state.tape[state.tpos + 1] + state.offset] + state.tape[state.tape[state.tpos + 2]];
      state.tpos += 4;
      break;
    case 1001:
      state.tape[state.tape[state.tpos + 3]] = state.tape[state.tape[state.tpos + 1]] + state.tape[state.tpos + 2];
      state.tpos += 4;
      break;
    case 1101:
      state.tape[state.tape[state.tpos + 3]] = state.tape[state.tpos + 1] + state.tape[state.tpos + 2];
      state.tpos += 4;
      break;
    case 1201:
      state.tape[state.tape[state.tpos + 3]] = state.tape[state.tape[state.tpos + 1] + state.offset] + state.tape[state.tpos + 2];
      state.tpos += 4;
      break;
    case 2001:
      state.tape[state.tape[state.tpos + 3]] = state.tape[state.tape[state.tpos + 1] + state.offset] + state.tape[state.tape[state.tpos + 2]];
      state.tpos += 4;
      break;
    case 2101:
      state.tape[state.tape[state.tpos + 3]] = state.tape[state.tpos + 1] + state.tape[state.tape[state.tpos + 2] + state.offset];
      state.tpos += 4;
      break;
    case 2201:
      state.tape[state.tape[state.tpos + 3]] = state.tape[state.tape[state.tpos + 1] + state.offset] + state.tape[state.tape[state.tpos + 2] + state.offset];
      state.tpos += 4;
      break;
    case 20001:
      state.tape[state.tape[state.tpos + 3] + state.offset] = state.tape[state.tape[state.tpos + 1]] + state.tape[state.tape[state.tpos + 2]];
      state.tpos += 4;
      break;
    case 20101:
      state.tape[state.tape[state.tpos + 3] + state.offset] = state.tape[state.tpos + 1] + state.tape[state.tape[state.tpos + 2]];
      state.tpos += 4;
      break;
    case 20201:
      state.tape[state.tape[state.tpos + 3] + state.offset] = state.tape[state.tape[state.tpos + 1] + state.offset] + state.tape[state.tape[state.tpos + 2]];
      state.tpos += 4;
      break;
    case 21001:
      state.tape[state.tape[state.tpos + 3] + state.offset] = state.tape[state.tape[state.tpos + 1]] + state.tape[state.tpos + 2];
      state.tpos += 4;
      break;
    case 21101:
      state.tape[state.tape[state.tpos + 3] + state.offset] = state.tape[state.tpos + 1] + state.tape[state.tpos + 2];
      state.tpos += 4;
      break;
    case 21201:
      state.tape[state.tape[state.tpos + 3] + state.offset] = state.tape[state.tape[state.tpos + 1] + state.offset] + state.tape[state.tpos + 2];
      state.tpos += 4;
      break;
    case 22001:
      state.tape[state.tape[state.tpos + 3] + state.offset] = state.tape[state.tape[state.tpos + 1] + state.offset] + state.tape[state.tape[state.tpos + 2]];
      state.tpos += 4;
      break;
    case 22101:
      state.tape[state.tape[state.tpos + 3] + state.offset] = state.tape[state.tpos + 1] + state.tape[state.tape[state.tpos + 2] + state.offset];
      state.tpos += 4;
      break;
    case 22201:
      state.tape[state.tape[state.tpos + 3] + state.offset] = state.tape[state.tape[state.tpos + 1] + state.offset] + state.tape[state.tape[state.tpos + 2] + state.offset];
      state.tpos += 4;
      break;

    //Opcode 2: MULT
    case 2:
      state.tape[state.tape[state.tpos + 3]] = state.tape[state.tape[state.tpos + 1]] * state.tape[state.tape[state.tpos + 2]];
      state.tpos += 4;
      break;
    case 102:
      state.tape[state.tape[state.tpos + 3]] = state.tape[state.tpos + 1] * state.tape[state.tape[state.tpos + 2]];
      state.tpos += 4;
      break;
    case 202:
      state.tape[state.tape[state.tpos + 3]] = state.tape[state.tape[state.tpos + 1] + state.offset] * state.tape[state.tape[state.tpos + 2]];
      state.tpos += 4;
      break;
    case 1002:
      state.tape[state.tape[state.tpos + 3]] = state.tape[state.tape[state.tpos + 1]] * state.tape[state.tpos + 2];
      state.tpos += 4;
      break;
    case 1102:
      state.tape[state.tape[state.tpos + 3]] = state.tape[state.tpos + 1] * state.tape[state.tpos + 2];
      state.tpos += 4;
      break;
    case 1202:
      state.tape[state.tape[state.tpos + 3]] = state.tape[state.tape[state.tpos + 1] + state.offset] * state.tape[state.tpos + 2];
      state.tpos += 4;
      break;
    case 2002:
      state.tape[state.tape[state.tpos + 3]] = state.tape[state.tape[state.tpos + 1]] * state.tape[state.tape[state.tpos + 2] + state.offset];
      state.tpos += 4;
      break;
    case 2102:
      state.tape[state.tape[state.tpos + 3]] = state.tape[state.tpos + 1] * state.tape[state.tape[state.tpos + 2] + state.offset];
      state.tpos += 4;
      break;
    case 2202:
      state.tape[state.tape[state.tpos + 3]] = state.tape[state.tape[state.tpos + 1] + state.offset] * state.tape[state.tape[state.tpos + 2] + state.offset];
      state.tpos += 4;
      break;
    case 20002:
      state.tape[state.tape[state.tpos + 3] + state.offset] = state.tape[state.tape[state.tpos + 1]] * state.tape[state.tape[state.tpos + 2]];
      state.tpos += 4;
      break;
    case 20102:
      state.tape[state.tape[state.tpos + 3] + state.offset] = state.tape[state.tpos + 1] * state.tape[state.tape[state.tpos + 2]];
      state.tpos += 4;
      break;
    case 20202:
      state.tape[state.tape[state.tpos + 3] + state.offset] = state.tape[state.tape[state.tpos + 1] + state.offset] * state.tape[state.tape[state.tpos + 2]];
      state.tpos += 4;
      break;
    case 21002:
      state.tape[state.tape[state.tpos + 3] + state.offset] = state.tape[state.tape[state.tpos + 1]] * state.tape[state.tpos + 2];
      state.tpos += 4;
      break;
    case 21102:
      state.tape[state.tape[state.tpos + 3] + state.offset] = state.tape[state.tpos + 1] * state.tape[state.tpos + 2];
      state.tpos += 4;
      break;
    case 21202:
      state.tape[state.tape[state.tpos + 3] + state.offset] = state.tape[state.tape[state.tpos + 1] + state.offset] * state.tape[state.tpos + 2];
      state.tpos += 4;
      break;
    case 22002:
      state.tape[state.tape[state.tpos + 3] + state.offset] = state.tape[state.tape[state.tpos + 1]] * state.tape[state.tape[state.tpos + 2] + state.offset];
      state.tpos += 4;
      break;
    case 22102:
      state.tape[state.tape[state.tpos + 3] + state.offset] = state.tape[state.tpos + 1] * state.tape[state.tape[state.tpos + 2] + state.offset];
      state.tpos += 4;
      break;
    case 22202:
      state.tape[state.tape[state.tpos + 3] + state.offset] = state.tape[state.tape[state.tpos + 1] + state.offset] * state.tape[state.tape[state.tpos + 2] + state.offset];
      state.tpos += 4;
      break;

    //Opcode 3: RDI
    case 3:
      if (state.bufferin) {
        char ch = '\0';
        bool sign = false;
        long num = 0;

        int ret = read(state.fin, &ch, 1);
        if (ret <= 0) {
          fprintf(stderr, "Need an input\n");
          exit(1);
        } else if (ch == '-') {
          sign = true;
          read(state.fin, &ch, 1);
        } else if (ch == '+') {
          read(state.fin, &ch, 1);
        }
        num = (long)(ch - '0');

        for (ret = read(state.fin, &ch, 1); ret == 1 && ch != '\n' && ch != ','; ret = read(state.fin, &ch, 1)) {
          num *= 10;
          num += (long)(ch - '0');
        }

        if (sign) {
          num = -num;
        }

        state.tape[state.tape[state.tpos + 1]] = num;
      } else {
        read(state.fin, &state.tape[state.tape[state.tpos + 1]], sizeof(state.tape[0]));
      }
      state.tpos += 2;
      break;
    case 203:
      if (state.bufferin) {
        char ch = '\0';
        bool sign = false;
        long num = 0;

        int ret = read(state.fin, &ch, 1);
        if (ret <= 0) {
          fprintf(stderr, "Need an input\n");
          exit(1);
        } else if (ch == '-') {
          sign = true;
          read(state.fin, &ch, 1);
        } else if (ch == '+') {
          read(state.fin, &ch, 1);
        }
        num = (long)(ch - '0');

        for (ret = read(state.fin, &ch, 1); ret == 1 && ch != '\n' && ch != ','; ret = read(state.fin, &ch, 1)) {
          num *= 10;
          num += (long)(ch - '0');
        }

        if (sign) {
          num = -num;
        }

        state.tape[state.tape[state.tpos + 1] + state.offset] = num;
      } else {
        read(state.fin, &state.tape[state.tape[state.tpos + 1] + state.offset], sizeof(state.tape[0]));
      }
      state.tpos += 2;
      break;

    //Opcode 4: PRI
    case 4:
      if (state.bufferout) {
        size_t bufpos = 0;
        bool neg = false;
        long out = state.tape[state.tape[state.tpos + 1]];
        if (out < 0) {
          out = -out;
          neg = true;
        }
        do {
          buffer[bufpos++] = ((char)(out % 10) + '0');
          out /= 10;
        } while (out);
        if (neg) {
          buffer[bufpos++] = '-';
        }
        buffer[bufpos++] = '\n';
        buffer[bufpos] = '\0';
        for (size_t i=0; i < (bufpos - 2) / 2; ++i) {
          char temp = buffer[i];
          buffer[i] = buffer[bufpos - 2 - i];
          buffer[bufpos - 2 - i] = temp;
        }
        write(state.fout, buffer, bufpos);
      } else {
        write(state.fout, &state.tape[state.tape[state.tpos + 1]], sizeof(state.tape[0]));
      }
      state.tpos += 2;
      break;
    case 104:
      if (state.bufferout) {
        size_t bufpos = 0;
        bool neg = false;
        long out = state.tape[state.tpos + 1];
        if (out < 0) {
          out = -out;
          neg = true;
        }
        do {
          buffer[bufpos++] = ((char)(out % 10) + '0');
          out /= 10;
        } while (out);
        if (neg) {
          buffer[bufpos++] = '-';
        }
        buffer[bufpos++] = '\n';
        buffer[bufpos] = '\0';

        for (size_t i=0; i < (bufpos - 2) / 2; ++i) {
          char temp = buffer[i];
          buffer[i] = buffer[bufpos - 2 - i];
          buffer[bufpos - 2 - i] = temp;
        }
        write(state.fout, buffer, bufpos);
      } else {
        write(state.fout, &state.tape[state.tpos + 1], sizeof(state.tape[0]));
      }
      state.tpos += 2;
      break;
      state.tpos += 2;
      break;
    case 204:
      if (state.bufferout) {
        size_t bufpos = 0;
        bool neg = false;
        long out = state.tape[state.tape[state.tpos + 1] + state.offset];
        if (out < 0) {
          out = -out;
          neg = true;
        }
        do {
          buffer[bufpos++] = ((char)(out % 10) + '0');
          out /= 10;
        } while (out);
        if (neg) {
          buffer[bufpos++] = '-';
        }
        buffer[bufpos++] = '\n';
        buffer[bufpos] = '\0';

        for (size_t i=0; i < (bufpos - 2) / 2; ++i) {
          char temp = buffer[i];
          buffer[i] = buffer[bufpos - 2 - i];
          buffer[bufpos - 2 - i] = temp;
        }
        write(state.fout, buffer, bufpos);
      } else {
        write(state.fout, &state.tape[state.tape[state.tpos + 1] + state.offset], sizeof(state.tape[0]));
      }
      state.tpos += 2;
      break;

    //Opcode 5: JNZ
    case 5:
      if (state.tape[state.tape[state.tpos + 1]]) {
        state.tpos = state.tape[state.tape[state.tpos + 2]];
      } else {
        state.tpos += 3;
      }
      break;
    case 105:
      if (state.tape[state.tpos + 1]) {
        state.tpos = state.tape[state.tape[state.tpos + 2]];
      } else {
        state.tpos += 3;
      }
      break;
    case 205:
      if (state.tape[state.tape[state.tpos + 1] + state.offset]) {
        state.tpos = state.tape[state.tape[state.tpos + 2]];
      } else {
        state.tpos += 3;
      }
      break;
    case 1005:
      if (state.tape[state.tape[state.tpos + 1]]) {
        state.tpos = state.tape[state.tpos + 2];
      } else {
        state.tpos += 3;
      }
      break;
    case 1105:
      if (state.tape[state.tpos + 1]) {
        state.tpos = state.tape[state.tpos + 2];
      } else {
        state.tpos += 3;
      }
      break;
    case 1205:
      if (state.tape[state.tape[state.tpos + 1] + state.offset]) {
        state.tpos = state.tape[state.tpos + 2];
      } else {
        state.tpos += 3;
      }
      break;
    case 2005:
      if (state.tape[state.tape[state.tpos + 1]]) {
        state.tpos = state.tape[state.tape[state.tpos + 2] + state.offset];
      } else {
        state.tpos += 3;
      }
      break;
    case 2105:
      if (state.tape[state.tpos + 1]) {
        state.tpos = state.tape[state.tape[state.tpos + 2] + state.offset];
      } else {
        state.tpos += 3;
      }
      break;
    case 2205:
      if (state.tape[state.offset + state.tape[state.tpos + 1]]) {
        state.tpos = state.tape[state.tape[state.tpos + 2] + state.offset];
      } else {
        state.tpos += 3;
      }
      break;

    // Opcode 6: JZ
    case 6:
      if (!state.tape[state.tape[state.tpos + 1]]) {
        state.tpos = state.tape[state.tape[state.tpos + 2]];
      } else {
        state.tpos += 3;
      }
      break;
    case 106:
      if (!state.tape[state.tpos + 1]) {
        state.tpos = state.tape[state.tape[state.tpos + 2]];
      } else {
        state.tpos += 3;
      }
      break;
    case 206:
      if (!state.tape[state.tape[state.tpos + 1] + state.offset]) {
        state.tpos = state.tape[state.tape[state.tpos + 2]];
      } else {
        state.tpos += 3;
      }
      break;
    case 1006:
      if (!state.tape[state.tape[state.tpos + 1]]) {
        state.tpos = state.tape[state.tpos + 2];
      } else {
        state.tpos += 3;
      }
      break;
    case 1106:
      if (!state.tape[state.tpos + 1]) {
        state.tpos = state.tape[state.tpos + 2];
      } else {
        state.tpos += 3;
      }
      break;
    case 1206:
      if (!state.tape[state.tape[state.tpos + 1] + state.offset]) {
        state.tpos = state.tape[state.tpos + 2];
      } else {
        state.tpos += 3;
      }
      break;
    case 2006:
      if (!state.tape[state.tape[state.tpos + 1]]) {
        state.tpos = state.tape[state.tape[state.tpos + 2] + state.offset];
      } else {
        state.tpos += 3;
      }
      break;
    case 2106:
      if (!state.tape[state.tpos + 1]) {
        state.tpos = state.tape[state.tape[state.tpos + 2] + state.offset];
      } else {
        state.tpos += 3;
      }
      break;
    case 2206:
      if (!state.tape[state.tape[state.tpos + 1] + state.offset]) {
        state.tpos = state.tape[state.tape[state.tpos + 2] + state.offset];
      } else {
        state.tpos += 3;
      }
      break;

    // Opcode 7: LT
    case 7:
      if (state.tape[state.tape[state.tpos + 1]] < state.tape[state.tape[state.tpos + 2]]) {
        state.tape[state.tape[state.tpos + 3]] = 1;
      } else {
        state.tape[state.tape[state.tpos + 3]] = 0;
      }
      state.tpos += 4;
      break;
    case 107:
      if (state.tape[state.tpos + 1] < state.tape[state.tape[state.tpos + 2]]) {
        state.tape[state.tape[state.tpos + 3]] = 1;
      } else {
        state.tape[state.tape[state.tpos + 3]] = 0;
      }
      state.tpos += 4;
      break;
    case 207:
      if (state.tape[state.tape[state.tpos + 1] + state.offset] < state.tape[state.tape[state.tpos + 2]]) {
        state.tape[state.tape[state.tpos + 3]] = 1;
      } else {
        state.tape[state.tape[state.tpos + 3]] = 0;
      }
      state.tpos += 4;
      break;
    case 1007:
      if (state.tape[state.tape[state.tpos + 1]] < state.tape[state.tpos + 2]) {
        state.tape[state.tape[state.tpos + 3]] = 1;
      } else {
        state.tape[state.tape[state.tpos + 3]] = 0;
      }
      state.tpos += 4;
      break;
    case 1107:
      if (state.tape[state.tpos + 1] < state.tape[state.tpos + 2]) {
        state.tape[state.tape[state.tpos + 3]] = 1;
      } else {
        state.tape[state.tape[state.tpos + 3]] = 0;
      }
      state.tpos += 4;
      break;
    case 1207:
      if (state.tape[state.tape[state.tpos + 1] + state.offset] < state.tape[state.tpos + 2]) {
        state.tape[state.tape[state.tpos + 3]] = 1;
      } else {
        state.tape[state.tape[state.tpos + 3]] = 0;
      }
      state.tpos += 4;
      break;
    case 2007:
      if (state.tape[state.tape[state.tpos + 1]] < state.tape[state.tape[state.tpos + 2] + state.offset]) {
        state.tape[state.tape[state.tpos + 3]] = 1;
      } else {
        state.tape[state.tape[state.tpos + 3]] = 0;
      }
      state.tpos += 4;
      break;
    case 2107:
      if (state.tape[state.tpos + 1] < state.tape[state.tape[state.tpos + 2] + state.offset]) {
        state.tape[state.tape[state.tpos + 3]] = 1;
      } else {
        state.tape[state.tape[state.tpos + 3]] = 0;
      }
      state.tpos += 4;
      break;
    case 2207:
      if (state.tape[state.offset + state.tape[state.tpos + 1]] < state.tape[state.tape[state.tpos + 2] + state.offset]) {
        state.tape[state.tape[state.tpos + 3]] = 1;
      } else {
        state.tape[state.tape[state.tpos + 3]] = 0;
      }
      state.tpos += 4;
      break;
    case 20007:
      if (state.tape[state.tape[state.tpos + 1]] < state.tape[state.tape[state.tpos + 2]]) {
        state.tape[state.tape[state.tpos + 3] + state.offset] = 1;
      } else {
        state.tape[state.tape[state.tpos + 3] + state.offset] = 0;
      }
      state.tpos += 4;
      break;
    case 20107:
      if (state.tape[state.tpos + 1] < state.tape[state.tape[state.tpos + 2]]) {
        state.tape[state.tape[state.tpos + 3] + state.offset] = 1;
      } else {
        state.tape[state.tape[state.tpos + 3] + state.offset] = 0;
      }
      state.tpos += 4;
      break;
    case 20207:
      if (state.tape[state.tape[state.tpos + 1] + state.offset] < state.tape[state.tape[state.tpos + 2]]) {
        state.tape[state.tape[state.tpos + 3] + state.offset] = 1;
      } else {
        state.tape[state.tape[state.tpos + 3] + state.offset] = 0;
      }
      state.tpos += 4;
      break;
    case 21007:
      if (state.tape[state.tape[state.tpos + 1]] < state.tape[state.tpos + 2]) {
        state.tape[state.tape[state.tpos + 3] + state.offset] = 1;
      } else {
        state.tape[state.tape[state.tpos + 3] + state.offset] = 0;
      }
      state.tpos += 4;
      break;
    case 21107:
      if (state.tape[state.tpos + 1] < state.tape[state.tpos + 2]) {
        state.tape[state.tape[state.tpos + 3] + state.offset] = 1;
      } else {
        state.tape[state.tape[state.tpos + 3] + state.offset] = 0;
      }
      state.tpos += 4;
      break;
    case 21207:
      if (state.tape[state.tape[state.tpos + 1] + state.offset] < state.tape[state.tpos + 2]) {
        state.tape[state.tape[state.tpos + 3] + state.offset] = 1;
      } else {
        state.tape[state.tape[state.tpos + 3] + state.offset] = 0;
      }
      state.tpos += 4;
      break;
    case 22007:
      if (state.tape[state.tape[state.tpos + 1]] < state.tape[state.tape[state.tpos + 2] + state.offset]) {
        state.tape[state.tape[state.tpos + 3] + state.offset] = 1;
      } else {
        state.tape[state.tape[state.tpos + 3] + state.offset] = 0;
      }
      state.tpos += 4;
      break;
    case 22107:
      if (state.tape[state.tpos + 1] < state.tape[state.tape[state.tpos + 2] + state.offset]) {
        state.tape[state.tape[state.tpos + 3] + state.offset] = 1;
      } else {
        state.tape[state.tape[state.tpos + 3] + state.offset] = 0;
      }
      state.tpos += 4;
      break;
    case 22207:
      if (state.tape[state.offset + state.tape[state.tpos + 1]] < state.tape[state.tape[state.tpos + 2] + state.offset]) {
        state.tape[state.tape[state.tpos + 3] + state.offset] = 1;
      } else {
        state.tape[state.tape[state.tpos + 3] + state.offset] = 0;
      }
      state.tpos += 4;
      break;

    // Opcode 8: EQ
    case 8:
      if (state.tape[state.tape[state.tpos + 1]] == state.tape[state.tape[state.tpos + 2]]) {
        state.tape[state.tape[state.tpos + 3]] = 1;
      } else {
        state.tape[state.tape[state.tpos + 3]] = 0;
      }
      state.tpos += 4;
      break;
    case 108:
      if (state.tape[state.tpos + 1] == state.tape[state.tape[state.tpos + 2]]) {
        state.tape[state.tape[state.tpos + 3]] = 1;
      } else {
        state.tape[state.tape[state.tpos + 3]] = 0;
      }
      state.tpos += 4;
      break;
    case 208:
      if (state.tape[state.tape[state.tpos + 1] + state.offset] == state.tape[state.tape[state.tpos + 2]]) {
        state.tape[state.tape[state.tpos + 3]] = 1;
      } else {
        state.tape[state.tape[state.tpos + 3]] = 0;
      }
      state.tpos += 4;
      break;
    case 1008:
      if (state.tape[state.tape[state.tpos + 1]] == state.tape[state.tpos + 2]) {
        state.tape[state.tape[state.tpos + 3]] = 1;
      } else {
        state.tape[state.tape[state.tpos + 3]] = 0;
      }
      state.tpos += 4;
      break;
    case 1108:
      if (state.tape[state.tpos + 1] == state.tape[state.tpos + 2]) {
        state.tape[state.tape[state.tpos + 3]] = 1;
      } else {
        state.tape[state.tape[state.tpos + 3]] = 0;
      }
      state.tpos += 4;
      break;
    case 1208:
      if (state.tape[state.offset + state.tape[state.tpos + 1]] == state.tape[state.tpos + 2]) {
        state.tape[state.tape[state.tpos + 3]] = 1;
      } else {
        state.tape[state.tape[state.tpos + 3]] = 0;
      }
      state.tpos += 4;
      break;
    case 2008:
      if (state.tape[state.tape[state.tpos + 1]] == state.tape[state.tape[state.tpos + 2] + state.offset]) {
        state.tape[state.tape[state.tpos + 3]] = 1;
      } else {
        state.tape[state.tape[state.tpos + 3]] = 0;
      }
      state.tpos += 4;
      break;
    case 2108:
      if (state.tape[state.tpos + 1] == state.tape[state.tape[state.tpos + 2] + state.offset]) {
        state.tape[state.tape[state.tpos + 3]] = 1;
      } else {
        state.tape[state.tape[state.tpos + 3]] = 0;
      }
      state.tpos += 4;
      break;
    case 2208:
      if (state.tape[state.tape[state.tpos + 1] + state.offset] == state.tape[state.tape[state.tpos + 2] + state.offset]) {
        state.tape[state.tape[state.tpos + 3]] = 1;
      } else {
        state.tape[state.tape[state.tpos + 3]] = 0;
      }
      state.tpos += 4;
      break;
    case 20008:
      if (state.tape[state.tape[state.tpos + 1]] == state.tape[state.tape[state.tpos + 2]]) {
        state.tape[state.tape[state.tpos + 3] + state.offset] = 1;
      } else {
        state.tape[state.tape[state.tpos + 3] + state.offset] = 0;
      }
      state.tpos += 4;
      break;
    case 20108:
      if (state.tape[state.tpos + 1] == state.tape[state.tape[state.tpos + 2]]) {
        state.tape[state.tape[state.tpos + 3] + state.offset] = 1;
      } else {
        state.tape[state.tape[state.tpos + 3] + state.offset] = 0;
      }
      state.tpos += 4;
      break;
    case 20208:
      if (state.tape[state.tape[state.tpos + 1] + state.offset] == state.tape[state.tape[state.tpos + 2]]) {
        state.tape[state.tape[state.tpos + 3] + state.offset] = 1;
      } else {
        state.tape[state.tape[state.tpos + 3] + state.offset] = 0;
      }
      state.tpos += 4;
      break;
    case 21008:
      if (state.tape[state.tape[state.tpos + 1]] == state.tape[state.tpos + 2]) {
        state.tape[state.tape[state.tpos + 3] + state.offset] = 1;
      } else {
        state.tape[state.tape[state.tpos + 3] + state.offset] = 0;
      }
      state.tpos += 4;
      break;
    case 21108:
      if (state.tape[state.tpos + 1] == state.tape[state.tpos + 2]) {
        state.tape[state.tape[state.tpos + 3] + state.offset] = 1;
      } else {
        state.tape[state.tape[state.tpos + 3] + state.offset] = 0;
      }
      state.tpos += 4;
      break;
    case 21208:
      if (state.tape[state.offset + state.tape[state.tpos + 1]] == state.tape[state.tpos + 2]) {
        state.tape[state.tape[state.tpos + 3] + state.offset] = 1;
      } else {
        state.tape[state.tape[state.tpos + 3] + state.offset] = 0;
      }
      state.tpos += 4;
      break;
    case 22008:
      if (state.tape[state.tape[state.tpos + 1]] == state.tape[state.tape[state.tpos + 2] + state.offset]) {
        state.tape[state.tape[state.tpos + 3] + state.offset] = 1;
      } else {
        state.tape[state.tape[state.tpos + 3] + state.offset] = 0;
      }
      state.tpos += 4;
      break;
    case 22108:
      if (state.tape[state.tpos + 1] == state.tape[state.tape[state.tpos + 2] + state.offset]) {
        state.tape[state.tape[state.tpos + 3] + state.offset] = 1;
      } else {
        state.tape[state.tape[state.tpos + 3] + state.offset] = 0;
      }
      state.tpos += 4;
      break;
    case 22208:
      if (state.tape[state.tape[state.tpos + 1] + state.offset] == state.tape[state.tape[state.tpos + 2] + state.offset]) {
        state.tape[state.tape[state.tpos + 3] + state.offset] = 1;
      } else {
        state.tape[state.tape[state.tpos + 3] + state.offset] = 0;
      }
      state.tpos += 4;
      break;

    // Opcode 9: REL
    case 9:
      state.offset += state.tape[state.tape[state.tpos + 1]];
      state.tpos += 2;
      break;
    case 109:
      state.offset += state.tape[state.tpos + 1];
      state.tpos += 2;
      break;
    case 209:
      state.offset += state.tape[state.tape[state.tpos + 1] + state.offset];
      state.tpos += 2;
      break;

    // No opcode
    default:
      fprintf(stderr, "Invalid opcode %ld\n", state.tape[state.tpos]);
      exit(1);
    }
  }
}

bool
intcode_step(struct intcode_state * state)
{
  char buffer[32];

  switch (state->tape[state->tpos]) {
  // Opcode 1: ADD
  case 1:
    state->tape[state->tape[state->tpos + 3]] = state->tape[state->tape[state->tpos + 1]] + state->tape[state->tape[state->tpos + 2]];
    state->tpos += 4;
    break;
  case 101:
    state->tape[state->tape[state->tpos + 3]] = state->tape[state->tpos + 1] + state->tape[state->tape[state->tpos + 2]];
    state->tpos += 4;
    break;
  case 201:
    state->tape[state->tape[state->tpos + 3]] = state->tape[state->tape[state->tpos + 1] + state->offset] + state->tape[state->tape[state->tpos + 2]];
    state->tpos += 4;
    break;
  case 1001:
    state->tape[state->tape[state->tpos + 3]] = state->tape[state->tape[state->tpos + 1]] + state->tape[state->tpos + 2];
    state->tpos += 4;
    break;
  case 1101:
    state->tape[state->tape[state->tpos + 3]] = state->tape[state->tpos + 1] + state->tape[state->tpos + 2];
    state->tpos += 4;
    break;
  case 1201:
    state->tape[state->tape[state->tpos + 3]] = state->tape[state->tape[state->tpos + 1] + state->offset] + state->tape[state->tpos + 2];
    state->tpos += 4;
    break;
  case 2001:
    state->tape[state->tape[state->tpos + 3]] = state->tape[state->tape[state->tpos + 1] + state->offset] + state->tape[state->tape[state->tpos + 2]];
    state->tpos += 4;
    break;
  case 2101:
    state->tape[state->tape[state->tpos + 3]] = state->tape[state->tpos + 1] + state->tape[state->tape[state->tpos + 2] + state->offset];
    state->tpos += 4;
    break;
  case 2201:
    state->tape[state->tape[state->tpos + 3]] = state->tape[state->tape[state->tpos + 1] + state->offset] + state->tape[state->tape[state->tpos + 2] + state->offset];
    state->tpos += 4;
    break;
  case 20001:
    state->tape[state->tape[state->tpos + 3] + state->offset] = state->tape[state->tape[state->tpos + 1]] + state->tape[state->tape[state->tpos + 2]];
    state->tpos += 4;
    break;
  case 20101:
    state->tape[state->tape[state->tpos + 3] + state->offset] = state->tape[state->tpos + 1] + state->tape[state->tape[state->tpos + 2]];
    state->tpos += 4;
    break;
  case 20201:
    state->tape[state->tape[state->tpos + 3] + state->offset] = state->tape[state->tape[state->tpos + 1] + state->offset] + state->tape[state->tape[state->tpos + 2]];
    state->tpos += 4;
    break;
  case 21001:
    state->tape[state->tape[state->tpos + 3] + state->offset] = state->tape[state->tape[state->tpos + 1]] + state->tape[state->tpos + 2];
    state->tpos += 4;
    break;
  case 21101:
    state->tape[state->tape[state->tpos + 3] + state->offset] = state->tape[state->tpos + 1] + state->tape[state->tpos + 2];
    state->tpos += 4;
    break;
  case 21201:
    state->tape[state->tape[state->tpos + 3] + state->offset] = state->tape[state->tape[state->tpos + 1] + state->offset] + state->tape[state->tpos + 2];
    state->tpos += 4;
    break;
  case 22001:
    state->tape[state->tape[state->tpos + 3] + state->offset] = state->tape[state->tape[state->tpos + 1] + state->offset] + state->tape[state->tape[state->tpos + 2]];
    state->tpos += 4;
    break;
  case 22101:
    state->tape[state->tape[state->tpos + 3] + state->offset] = state->tape[state->tpos + 1] + state->tape[state->tape[state->tpos + 2] + state->offset];
    state->tpos += 4;
    break;
  case 22201:
    state->tape[state->tape[state->tpos + 3] + state->offset] = state->tape[state->tape[state->tpos + 1] + state->offset] + state->tape[state->tape[state->tpos + 2] + state->offset];
    state->tpos += 4;
    break;

  //Opcode 2: MULT
  case 2:
    state->tape[state->tape[state->tpos + 3]] = state->tape[state->tape[state->tpos + 1]] * state->tape[state->tape[state->tpos + 2]];
    state->tpos += 4;
    break;
  case 102:
    state->tape[state->tape[state->tpos + 3]] = state->tape[state->tpos + 1] * state->tape[state->tape[state->tpos + 2]];
    state->tpos += 4;
    break;
  case 202:
    state->tape[state->tape[state->tpos + 3]] = state->tape[state->tape[state->tpos + 1] + state->offset] * state->tape[state->tape[state->tpos + 2]];
    state->tpos += 4;
    break;
  case 1002:
    state->tape[state->tape[state->tpos + 3]] = state->tape[state->tape[state->tpos + 1]] * state->tape[state->tpos + 2];
    state->tpos += 4;
    break;
  case 1102:
    state->tape[state->tape[state->tpos + 3]] = state->tape[state->tpos + 1] * state->tape[state->tpos + 2];
    state->tpos += 4;
    break;
  case 1202:
    state->tape[state->tape[state->tpos + 3]] = state->tape[state->tape[state->tpos + 1] + state->offset] * state->tape[state->tpos + 2];
    state->tpos += 4;
    break;
  case 2002:
    state->tape[state->tape[state->tpos + 3]] = state->tape[state->tape[state->tpos + 1]] * state->tape[state->tape[state->tpos + 2] + state->offset];
    state->tpos += 4;
    break;
  case 2102:
    state->tape[state->tape[state->tpos + 3]] = state->tape[state->tpos + 1] * state->tape[state->tape[state->tpos + 2] + state->offset];
    state->tpos += 4;
    break;
  case 2202:
    state->tape[state->tape[state->tpos + 3]] = state->tape[state->tape[state->tpos + 1] + state->offset] * state->tape[state->tape[state->tpos + 2] + state->offset];
    state->tpos += 4;
    break;
  case 20002:
    state->tape[state->tape[state->tpos + 3] + state->offset] = state->tape[state->tape[state->tpos + 1]] * state->tape[state->tape[state->tpos + 2]];
    state->tpos += 4;
    break;
  case 20102:
    state->tape[state->tape[state->tpos + 3] + state->offset] = state->tape[state->tpos + 1] * state->tape[state->tape[state->tpos + 2]];
    state->tpos += 4;
    break;
  case 20202:
    state->tape[state->tape[state->tpos + 3] + state->offset] = state->tape[state->tape[state->tpos + 1] + state->offset] * state->tape[state->tape[state->tpos + 2]];
    state->tpos += 4;
    break;
  case 21002:
    state->tape[state->tape[state->tpos + 3] + state->offset] = state->tape[state->tape[state->tpos + 1]] * state->tape[state->tpos + 2];
    state->tpos += 4;
    break;
  case 21102:
    state->tape[state->tape[state->tpos + 3] + state->offset] = state->tape[state->tpos + 1] * state->tape[state->tpos + 2];
    state->tpos += 4;
    break;
  case 21202:
    state->tape[state->tape[state->tpos + 3] + state->offset] = state->tape[state->tape[state->tpos + 1] + state->offset] * state->tape[state->tpos + 2];
    state->tpos += 4;
    break;
  case 22002:
    state->tape[state->tape[state->tpos + 3] + state->offset] = state->tape[state->tape[state->tpos + 1]] * state->tape[state->tape[state->tpos + 2] + state->offset];
    state->tpos += 4;
    break;
  case 22102:
    state->tape[state->tape[state->tpos + 3] + state->offset] = state->tape[state->tpos + 1] * state->tape[state->tape[state->tpos + 2] + state->offset];
    state->tpos += 4;
    break;
  case 22202:
    state->tape[state->tape[state->tpos + 3] + state->offset] = state->tape[state->tape[state->tpos + 1] + state->offset] * state->tape[state->tape[state->tpos + 2] + state->offset];
    state->tpos += 4;
    break;

  //Opcode 3: RDI
  case 3:
    if (state->bufferin) {
      char ch = '\0';
      bool sign = false;
      long num = 0;

      int ret = read(state->fin, &ch, 1);
      if (ret <= 0) {
        fprintf(stderr, "Need an input\n");
        exit(1);
      } else if (ch == '-') {
        sign = true;
        read(state->fin, &ch, 1);
      } else if (ch == '+') {
        read(state->fin, &ch, 1);
      }
      num = (long)(ch - '0');

      for (ret = read(state->fin, &ch, 1); ret == 1 && ch != '\n' && ch != ','; ret = read(state->fin, &ch, 1)) {
        num *= 10;
        num += (long)(ch - '0');
      }

      if (sign) {
        num = -num;
      }

      state->tape[state->tape[state->tpos + 1]] = num;
    } else {
      read(state->fin, &state->tape[state->tape[state->tpos + 1]], sizeof(state->tape[0]));
    }
    state->tpos += 2;
    break;
  case 203:
    if (state->bufferin) {
      char ch = '\0';
      bool sign = false;
      long num = 0;

      int ret = read(state->fin, &ch, 1);
      if (ret <= 0) {
        fprintf(stderr, "Need an input\n");
        exit(1);
      } else if (ch == '-') {
        sign = true;
        read(state->fin, &ch, 1);
      } else if (ch == '+') {
        read(state->fin, &ch, 1);
      }
      num = (long)(ch - '0');

      for (ret = read(state->fin, &ch, 1); ret == 1 && ch != '\n' && ch != ','; ret = read(state->fin, &ch, 1)) {
        num *= 10;
        num += (long)(ch - '0');
      }

      if (sign) {
        num = -num;
      }

      state->tape[state->tape[state->tpos + 1] + state->offset] = num;
    } else {
      read(state->fin, &state->tape[state->tape[state->tpos + 1] + state->offset], sizeof(state->tape[0]));
    }
    state->tpos += 2;
    break;

  //Opcode 4: PRI
  case 4:
    if (state->bufferout) {
      size_t bufpos = 0;
      bool neg = false;
      long out = state->tape[state->tape[state->tpos + 1]];
      if (out < 0) {
        out = -out;
        neg = true;
      }
      do {
        buffer[bufpos++] = ((char)(out % 10) + '0');
        out /= 10;
      } while (out);
      if (neg) {
        buffer[bufpos++] = '-';
      }
      buffer[bufpos++] = '\n';
      buffer[bufpos] = '\0';
      for (size_t i=0; i < (bufpos - 2) / 2; ++i) {
        char temp = buffer[i];
        buffer[i] = buffer[bufpos - 2 - i];
        buffer[bufpos - 2 - i] = temp;
      }
      write(state->fout, buffer, bufpos);
    } else {
      write(state->fout, &state->tape[state->tape[state->tpos + 1]], sizeof(state->tape[0]));
    }
    state->tpos += 2;
    break;
  case 104:
    if (state->bufferout) {
      size_t bufpos = 0;
      bool neg = false;
      long out = state->tape[state->tpos + 1];
      if (out < 0) {
        out = -out;
        neg = true;
      }
      do {
        buffer[bufpos++] = ((char)(out % 10) + '0');
        out /= 10;
      } while (out);
      if (neg) {
        buffer[bufpos++] = '-';
      }
      buffer[bufpos++] = '\n';
      buffer[bufpos] = '\0';

      for (size_t i=0; i < (bufpos - 2) / 2; ++i) {
        char temp = buffer[i];
        buffer[i] = buffer[bufpos - 2 - i];
        buffer[bufpos - 2 - i] = temp;
      }
      write(state->fout, buffer, bufpos);
    } else {
      write(state->fout, &state->tape[state->tpos + 1], sizeof(state->tape[0]));
    }
    state->tpos += 2;
    break;
    state->tpos += 2;
    break;
  case 204:
    if (state->bufferout) {
      size_t bufpos = 0;
      bool neg = false;
      long out = state->tape[state->tape[state->tpos + 1] + state->offset];
      if (out < 0) {
        out = -out;
        neg = true;
      }
      do {
        buffer[bufpos++] = ((char)(out % 10) + '0');
        out /= 10;
      } while (out);
      if (neg) {
        buffer[bufpos++] = '-';
      }
      buffer[bufpos++] = '\n';
      buffer[bufpos] = '\0';

      for (size_t i=0; i < (bufpos - 2) / 2; ++i) {
        char temp = buffer[i];
        buffer[i] = buffer[bufpos - 2 - i];
        buffer[bufpos - 2 - i] = temp;
      }
      write(state->fout, buffer, bufpos);
    } else {
      write(state->fout, &state->tape[state->tape[state->tpos + 1] + state->offset], sizeof(state->tape[0]));
    }
    state->tpos += 2;
    break;

  //Opcode 5: JNZ
  case 5:
    if (state->tape[state->tape[state->tpos + 1]]) {
      state->tpos = state->tape[state->tape[state->tpos + 2]];
    } else {
      state->tpos += 3;
    }
    break;
  case 105:
    if (state->tape[state->tpos + 1]) {
      state->tpos = state->tape[state->tape[state->tpos + 2]];
    } else {
      state->tpos += 3;
    }
    break;
  case 205:
    if (state->tape[state->tape[state->tpos + 1] + state->offset]) {
      state->tpos = state->tape[state->tape[state->tpos + 2]];
    } else {
      state->tpos += 3;
    }
    break;
  case 1005:
    if (state->tape[state->tape[state->tpos + 1]]) {
      state->tpos = state->tape[state->tpos + 2];
    } else {
      state->tpos += 3;
    }
    break;
  case 1105:
    if (state->tape[state->tpos + 1]) {
      state->tpos = state->tape[state->tpos + 2];
    } else {
      state->tpos += 3;
    }
    break;
  case 1205:
    if (state->tape[state->tape[state->tpos + 1] + state->offset]) {
      state->tpos = state->tape[state->tpos + 2];
    } else {
      state->tpos += 3;
    }
    break;
  case 2005:
    if (state->tape[state->tape[state->tpos + 1]]) {
      state->tpos = state->tape[state->tape[state->tpos + 2] + state->offset];
    } else {
      state->tpos += 3;
    }
    break;
  case 2105:
    if (state->tape[state->tpos + 1]) {
      state->tpos = state->tape[state->tape[state->tpos + 2] + state->offset];
    } else {
      state->tpos += 3;
    }
    break;
  case 2205:
    if (state->tape[state->offset + state->tape[state->tpos + 1]]) {
      state->tpos = state->tape[state->tape[state->tpos + 2] + state->offset];
    } else {
      state->tpos += 3;
    }
    break;

  // Opcode 6: JZ
  case 6:
    if (!state->tape[state->tape[state->tpos + 1]]) {
      state->tpos = state->tape[state->tape[state->tpos + 2]];
    } else {
      state->tpos += 3;
    }
    break;
  case 106:
    if (!state->tape[state->tpos + 1]) {
      state->tpos = state->tape[state->tape[state->tpos + 2]];
    } else {
      state->tpos += 3;
    }
    break;
  case 206:
    if (!state->tape[state->tape[state->tpos + 1] + state->offset]) {
      state->tpos = state->tape[state->tape[state->tpos + 2]];
    } else {
      state->tpos += 3;
    }
    break;
  case 1006:
    if (!state->tape[state->tape[state->tpos + 1]]) {
      state->tpos = state->tape[state->tpos + 2];
    } else {
      state->tpos += 3;
    }
    break;
  case 1106:
    if (!state->tape[state->tpos + 1]) {
      state->tpos = state->tape[state->tpos + 2];
    } else {
      state->tpos += 3;
    }
    break;
  case 1206:
    if (!state->tape[state->tape[state->tpos + 1] + state->offset]) {
      state->tpos = state->tape[state->tpos + 2];
    } else {
      state->tpos += 3;
    }
    break;
  case 2006:
    if (!state->tape[state->tape[state->tpos + 1]]) {
      state->tpos = state->tape[state->tape[state->tpos + 2] + state->offset];
    } else {
      state->tpos += 3;
    }
    break;
  case 2106:
    if (!state->tape[state->tpos + 1]) {
      state->tpos = state->tape[state->tape[state->tpos + 2] + state->offset];
    } else {
      state->tpos += 3;
    }
    break;
  case 2206:
    if (!state->tape[state->tape[state->tpos + 1] + state->offset]) {
      state->tpos = state->tape[state->tape[state->tpos + 2] + state->offset];
    } else {
      state->tpos += 3;
    }
    break;

  // Opcode 7: LT
  case 7:
    if (state->tape[state->tape[state->tpos + 1]] < state->tape[state->tape[state->tpos + 2]]) {
      state->tape[state->tape[state->tpos + 3]] = 1;
    } else {
      state->tape[state->tape[state->tpos + 3]] = 0;
    }
    state->tpos += 4;
    break;
  case 107:
    if (state->tape[state->tpos + 1] < state->tape[state->tape[state->tpos + 2]]) {
      state->tape[state->tape[state->tpos + 3]] = 1;
    } else {
      state->tape[state->tape[state->tpos + 3]] = 0;
    }
    state->tpos += 4;
    break;
  case 207:
    if (state->tape[state->tape[state->tpos + 1] + state->offset] < state->tape[state->tape[state->tpos + 2]]) {
      state->tape[state->tape[state->tpos + 3]] = 1;
    } else {
      state->tape[state->tape[state->tpos + 3]] = 0;
    }
    state->tpos += 4;
    break;
  case 1007:
    if (state->tape[state->tape[state->tpos + 1]] < state->tape[state->tpos + 2]) {
      state->tape[state->tape[state->tpos + 3]] = 1;
    } else {
      state->tape[state->tape[state->tpos + 3]] = 0;
    }
    state->tpos += 4;
    break;
  case 1107:
    if (state->tape[state->tpos + 1] < state->tape[state->tpos + 2]) {
      state->tape[state->tape[state->tpos + 3]] = 1;
    } else {
      state->tape[state->tape[state->tpos + 3]] = 0;
    }
    state->tpos += 4;
    break;
  case 1207:
    if (state->tape[state->tape[state->tpos + 1] + state->offset] < state->tape[state->tpos + 2]) {
      state->tape[state->tape[state->tpos + 3]] = 1;
    } else {
      state->tape[state->tape[state->tpos + 3]] = 0;
    }
    state->tpos += 4;
    break;
  case 2007:
    if (state->tape[state->tape[state->tpos + 1]] < state->tape[state->tape[state->tpos + 2] + state->offset]) {
      state->tape[state->tape[state->tpos + 3]] = 1;
    } else {
      state->tape[state->tape[state->tpos + 3]] = 0;
    }
    state->tpos += 4;
    break;
  case 2107:
    if (state->tape[state->tpos + 1] < state->tape[state->tape[state->tpos + 2] + state->offset]) {
      state->tape[state->tape[state->tpos + 3]] = 1;
    } else {
      state->tape[state->tape[state->tpos + 3]] = 0;
    }
    state->tpos += 4;
    break;
  case 2207:
    if (state->tape[state->offset + state->tape[state->tpos + 1]] < state->tape[state->tape[state->tpos + 2] + state->offset]) {
      state->tape[state->tape[state->tpos + 3]] = 1;
    } else {
      state->tape[state->tape[state->tpos + 3]] = 0;
    }
    state->tpos += 4;
    break;
  case 20007:
    if (state->tape[state->tape[state->tpos + 1]] < state->tape[state->tape[state->tpos + 2]]) {
      state->tape[state->tape[state->tpos + 3] + state->offset] = 1;
    } else {
      state->tape[state->tape[state->tpos + 3] + state->offset] = 0;
    }
    state->tpos += 4;
    break;
  case 20107:
    if (state->tape[state->tpos + 1] < state->tape[state->tape[state->tpos + 2]]) {
      state->tape[state->tape[state->tpos + 3] + state->offset] = 1;
    } else {
      state->tape[state->tape[state->tpos + 3] + state->offset] = 0;
    }
    state->tpos += 4;
    break;
  case 20207:
    if (state->tape[state->tape[state->tpos + 1] + state->offset] < state->tape[state->tape[state->tpos + 2]]) {
      state->tape[state->tape[state->tpos + 3] + state->offset] = 1;
    } else {
      state->tape[state->tape[state->tpos + 3] + state->offset] = 0;
    }
    state->tpos += 4;
    break;
  case 21007:
    if (state->tape[state->tape[state->tpos + 1]] < state->tape[state->tpos + 2]) {
      state->tape[state->tape[state->tpos + 3] + state->offset] = 1;
    } else {
      state->tape[state->tape[state->tpos + 3] + state->offset] = 0;
    }
    state->tpos += 4;
    break;
  case 21107:
    if (state->tape[state->tpos + 1] < state->tape[state->tpos + 2]) {
      state->tape[state->tape[state->tpos + 3] + state->offset] = 1;
    } else {
      state->tape[state->tape[state->tpos + 3] + state->offset] = 0;
    }
    state->tpos += 4;
    break;
  case 21207:
    if (state->tape[state->tape[state->tpos + 1] + state->offset] < state->tape[state->tpos + 2]) {
      state->tape[state->tape[state->tpos + 3] + state->offset] = 1;
    } else {
      state->tape[state->tape[state->tpos + 3] + state->offset] = 0;
    }
    state->tpos += 4;
    break;
  case 22007:
    if (state->tape[state->tape[state->tpos + 1]] < state->tape[state->tape[state->tpos + 2] + state->offset]) {
      state->tape[state->tape[state->tpos + 3] + state->offset] = 1;
    } else {
      state->tape[state->tape[state->tpos + 3] + state->offset] = 0;
    }
    state->tpos += 4;
    break;
  case 22107:
    if (state->tape[state->tpos + 1] < state->tape[state->tape[state->tpos + 2] + state->offset]) {
      state->tape[state->tape[state->tpos + 3] + state->offset] = 1;
    } else {
      state->tape[state->tape[state->tpos + 3] + state->offset] = 0;
    }
    state->tpos += 4;
    break;
  case 22207:
    if (state->tape[state->offset + state->tape[state->tpos + 1]] < state->tape[state->tape[state->tpos + 2] + state->offset]) {
      state->tape[state->tape[state->tpos + 3] + state->offset] = 1;
    } else {
      state->tape[state->tape[state->tpos + 3] + state->offset] = 0;
    }
    state->tpos += 4;
    break;

  // Opcode 8: EQ
  case 8:
    if (state->tape[state->tape[state->tpos + 1]] == state->tape[state->tape[state->tpos + 2]]) {
      state->tape[state->tape[state->tpos + 3]] = 1;
    } else {
      state->tape[state->tape[state->tpos + 3]] = 0;
    }
    state->tpos += 4;
    break;
  case 108:
    if (state->tape[state->tpos + 1] == state->tape[state->tape[state->tpos + 2]]) {
      state->tape[state->tape[state->tpos + 3]] = 1;
    } else {
      state->tape[state->tape[state->tpos + 3]] = 0;
    }
    state->tpos += 4;
    break;
  case 208:
    if (state->tape[state->tape[state->tpos + 1] + state->offset] == state->tape[state->tape[state->tpos + 2]]) {
      state->tape[state->tape[state->tpos + 3]] = 1;
    } else {
      state->tape[state->tape[state->tpos + 3]] = 0;
    }
    state->tpos += 4;
    break;
  case 1008:
    if (state->tape[state->tape[state->tpos + 1]] == state->tape[state->tpos + 2]) {
      state->tape[state->tape[state->tpos + 3]] = 1;
    } else {
      state->tape[state->tape[state->tpos + 3]] = 0;
    }
    state->tpos += 4;
    break;
  case 1108:
    if (state->tape[state->tpos + 1] == state->tape[state->tpos + 2]) {
      state->tape[state->tape[state->tpos + 3]] = 1;
    } else {
      state->tape[state->tape[state->tpos + 3]] = 0;
    }
    state->tpos += 4;
    break;
  case 1208:
    if (state->tape[state->offset + state->tape[state->tpos + 1]] == state->tape[state->tpos + 2]) {
      state->tape[state->tape[state->tpos + 3]] = 1;
    } else {
      state->tape[state->tape[state->tpos + 3]] = 0;
    }
    state->tpos += 4;
    break;
  case 2008:
    if (state->tape[state->tape[state->tpos + 1]] == state->tape[state->tape[state->tpos + 2] + state->offset]) {
      state->tape[state->tape[state->tpos + 3]] = 1;
    } else {
      state->tape[state->tape[state->tpos + 3]] = 0;
    }
    state->tpos += 4;
    break;
  case 2108:
    if (state->tape[state->tpos + 1] == state->tape[state->tape[state->tpos + 2] + state->offset]) {
      state->tape[state->tape[state->tpos + 3]] = 1;
    } else {
      state->tape[state->tape[state->tpos + 3]] = 0;
    }
    state->tpos += 4;
    break;
  case 2208:
    if (state->tape[state->tape[state->tpos + 1] + state->offset] == state->tape[state->tape[state->tpos + 2] + state->offset]) {
      state->tape[state->tape[state->tpos + 3]] = 1;
    } else {
      state->tape[state->tape[state->tpos + 3]] = 0;
    }
    state->tpos += 4;
    break;
  case 20008:
    if (state->tape[state->tape[state->tpos + 1]] == state->tape[state->tape[state->tpos + 2]]) {
      state->tape[state->tape[state->tpos + 3] + state->offset] = 1;
    } else {
      state->tape[state->tape[state->tpos + 3] + state->offset] = 0;
    }
    state->tpos += 4;
    break;
  case 20108:
    if (state->tape[state->tpos + 1] == state->tape[state->tape[state->tpos + 2]]) {
      state->tape[state->tape[state->tpos + 3] + state->offset] = 1;
    } else {
      state->tape[state->tape[state->tpos + 3] + state->offset] = 0;
    }
    state->tpos += 4;
    break;
  case 20208:
    if (state->tape[state->tape[state->tpos + 1] + state->offset] == state->tape[state->tape[state->tpos + 2]]) {
      state->tape[state->tape[state->tpos + 3] + state->offset] = 1;
    } else {
      state->tape[state->tape[state->tpos + 3] + state->offset] = 0;
    }
    state->tpos += 4;
    break;
  case 21008:
    if (state->tape[state->tape[state->tpos + 1]] == state->tape[state->tpos + 2]) {
      state->tape[state->tape[state->tpos + 3] + state->offset] = 1;
    } else {
      state->tape[state->tape[state->tpos + 3] + state->offset] = 0;
    }
    state->tpos += 4;
    break;
  case 21108:
    if (state->tape[state->tpos + 1] == state->tape[state->tpos + 2]) {
      state->tape[state->tape[state->tpos + 3] + state->offset] = 1;
    } else {
      state->tape[state->tape[state->tpos + 3] + state->offset] = 0;
    }
    state->tpos += 4;
    break;
  case 21208:
    if (state->tape[state->offset + state->tape[state->tpos + 1]] == state->tape[state->tpos + 2]) {
      state->tape[state->tape[state->tpos + 3] + state->offset] = 1;
    } else {
      state->tape[state->tape[state->tpos + 3] + state->offset] = 0;
    }
    state->tpos += 4;
    break;
  case 22008:
    if (state->tape[state->tape[state->tpos + 1]] == state->tape[state->tape[state->tpos + 2] + state->offset]) {
      state->tape[state->tape[state->tpos + 3] + state->offset] = 1;
    } else {
      state->tape[state->tape[state->tpos + 3] + state->offset] = 0;
    }
    state->tpos += 4;
    break;
  case 22108:
    if (state->tape[state->tpos + 1] == state->tape[state->tape[state->tpos + 2] + state->offset]) {
      state->tape[state->tape[state->tpos + 3] + state->offset] = 1;
    } else {
      state->tape[state->tape[state->tpos + 3] + state->offset] = 0;
    }
    state->tpos += 4;
    break;
  case 22208:
    if (state->tape[state->tape[state->tpos + 1] + state->offset] == state->tape[state->tape[state->tpos + 2] + state->offset]) {
      state->tape[state->tape[state->tpos + 3] + state->offset] = 1;
    } else {
      state->tape[state->tape[state->tpos + 3] + state->offset] = 0;
    }
    state->tpos += 4;
    break;

  // Opcode 9: REL
  case 9:
    state->offset += state->tape[state->tape[state->tpos + 1]];
    state->tpos += 2;
    break;
  case 109:
    state->offset += state->tape[state->tpos + 1];
    state->tpos += 2;
    break;
  case 209:
    state->offset += state->tape[state->tape[state->tpos + 1] + state->offset];
    state->tpos += 2;
    break;

  case 99:
    return true;

  // No opcode
  default:
    fprintf(stderr, "Invalid opcode %ld\n", state->tape[state->tpos]);
    exit(1);
  }

  return false;
}
