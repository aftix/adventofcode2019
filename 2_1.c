#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "timing.h"

int
main(int argc, char ** argv)
{
  uint64_t start = ns();
  int tape[145];
  memset(tape, 0, sizeof(tape));

  size_t tpos = 0;
  int inc = 0;

  tape[0] = (getchar() - (int)'0');

  for (inc = getchar(); inc != EOF; inc = getchar()) {
    if (inc == (int)',') {
      tape[++tpos] = (getchar() - (int)'0');
      continue;
    }

    tape[tpos] *= 10;
    tape[tpos] += (inc - (int)'0');
  }

  tpos = 0;
  while (tape[tpos] != 99) {
    switch (tape[tpos]) {
    case 1:
      tape[tape[tpos + 3]] = tape[tape[tpos + 1]] + tape[tape[tpos + 2]];
      break;
    case 2:
      tape[tape[tpos + 3]] = tape[tape[tpos + 1]] * tape[tape[tpos + 2]];
      break;
    default:
      fprintf(stderr, "Invalid opcode %d\n", tape[tpos]);
      return 1;
    }

    tpos += 4;
  }

  printf("%d\n", tape[0]);

  showtiming(start);

  return 0;
}
