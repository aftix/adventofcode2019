#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <limits.h>

#include "timing.h"

const int width = 25;
const int height = 6;
const int maxlayers = 128;

int
main(int argc, char ** argv)
{
  uint64_t start = ns();

  int numzeros = 0;
  int numones = 0;
  int numtwos = 0;

  int inc = 0;
  int lcount = 0;

  int minzeros = INT_MAX;
  int minones = 0;
  int mintwos = 0;

  for (inc = getchar(); inc != EOF; inc = getchar()) {
    switch (inc) {
    case '0':
      ++numzeros;
      break;
    case '1':
      ++numones;
      break;
    case '2':
      ++numtwos;
      break;
    case '\n':
      continue;
    }

    ++lcount;

    if (lcount >= width * height) {
      lcount = 0;
      if (numzeros < minzeros) {
        minzeros = numzeros;
        minones = numones;
        mintwos = numtwos;
      }

      numzeros = 0;
      numones = 0;
      numtwos = 0;
    }
  }

  printf("%d\n", minones * mintwos);

  showtiming(start);
  return 0;
}
