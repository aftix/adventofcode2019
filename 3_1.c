#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>

#include "timing.h"

struct segment {
  int x1, y1;
  int mx, my;
};

int
main(int argc, char ** argv)
{
  uint64_t start = ns();

  int inc = 0;

  struct segment wire1[1024];
  struct segment wire2[1024];

  size_t wire1pos = 1;
  size_t wire2pos = 1;

  wire1[0].x1 = 0;
  wire1[0].y1 = 0;
  wire1[0].mx = 0;
  wire1[0].my = 0;

  while (true) {
    inc = getchar();
    char direction = (char)inc;

    int length = (getchar() - (int)'0');
    for (inc = getchar(); inc != (int)',' && inc != (int)'\n'; inc = getchar()) {
      length *= 10;
      length += (inc - (int)'0');
    }

    wire1[wire1pos].x1 = wire1[wire1pos - 1].x1 + wire1[wire1pos - 1].mx;
    wire1[wire1pos].y1 = wire1[wire1pos - 1].y1 + wire1[wire1pos - 1].my;

    switch (direction) {
    case 'R':
      wire1[wire1pos].mx = length;
      wire1[wire1pos].my = 0;
      break;
    case 'L':
      wire1[wire1pos].mx = -length;
      wire1[wire1pos].my = 0;
      break;
    case 'U':
      wire1[wire1pos].mx = 0;
      wire1[wire1pos].my = length;
      break;
    case 'D':
      wire1[wire1pos].mx = 0;
      wire1[wire1pos].my = -length;
      break;
    }

    wire1pos++;

    if (inc == (int)'\n' || inc == EOF) {
      break;
    }
  }

  wire2[0].x1 = 0;
  wire2[0].y1 = 0;
  wire2[0].mx = 0;
  wire2[0].my = 0;

  while (true) {
    inc = getchar();
    char direction = (char)inc;

    int length = (getchar() - (int)'0');
    for (inc = getchar(); inc != (int)',' && inc != (int)'\n' && inc != EOF; inc = getchar()) {
      length *= 10;
      length += (inc - (int)'0');
    }

    wire2[wire2pos].x1 = wire2[wire2pos - 1].x1 + wire2[wire2pos - 1].mx;
    wire2[wire2pos].y1 = wire2[wire2pos - 1].y1 + wire2[wire2pos - 1].my;

    switch (direction) {
    case 'R':
      wire2[wire2pos].mx = length;
      wire2[wire2pos].my = 0;
      break;
    case 'L':
      wire2[wire2pos].mx = -length;
      wire2[wire2pos].my = 0;
      break;
    case 'U':
      wire2[wire2pos].mx = 0;
      wire2[wire2pos].my = length;
      break;
    case 'D':
      wire2[wire2pos].mx = 0;
      wire2[wire2pos].my = -length;
      break;
    }

    wire2pos++;

    if (inc == EOF || inc == (int)'\n') {
      break;
    }
  }

  unsigned int mindist = UINT_MAX;
  int minx = 0;
  int miny = 0;

  for (size_t i=0; i < wire1pos; ++i) {
    for (size_t j=0; j < wire2pos; ++j) {
      const int crossx = wire1[i].x1 - wire2[j].x1;
      const int crossy = wire1[i].y1 - wire2[j].y1;

      int denom = wire1[i].mx * wire2[j].my - wire1[i].my * wire2[j].mx;

      const int t = (wire2[j].mx * crossy - wire2[j].my * crossx) * ((denom > 0) - (denom < 0));
      const int u = (wire1[i].mx * crossy - wire1[i].my * crossx) * ((denom > 0) - (denom < 0));
      denom *= ((denom > 0) - (denom < 0));

      if (t <= 0 || t >= denom) {
        continue;
      }
      if (u <= 0 || u >= denom) {
        continue;
      }


      const int x = wire1[i].x1 + (t * wire1[i].mx) / denom;
      const int y = wire1[i].y1 + (t * wire1[i].my) / denom;

      const unsigned int dist = abs(x) + abs(y);

      if (dist && dist < mindist) {
        mindist = dist;
        minx = x;
        miny = y;
      }
    }
  }

  printf("(%d,%d): %u\n", minx, miny, mindist);

  showtiming(start);

  return 0;
}
