#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "timing.h"

int tape[145];

void
simulate()
{
  size_t tpos = 0;
  while (tape[tpos] != 99) {
    switch (tape[tpos]) {
    case 1:
      tape[tape[tpos + 3]] = tape[tape[tpos + 1]] + tape[tape[tpos + 2]];
      break;
    case 2:
      tape[tape[tpos + 3]] = tape[tape[tpos + 1]] * tape[tape[tpos + 2]];
      break;
    default:
      fprintf(stderr, "Invalid opcode %d\n", tape[tpos]);
      exit(1);
    }

    tpos += 4;
  }
}

int
main(int argc, char ** argv)
{
  uint64_t start = ns();

  memset(tape, 0, sizeof(tape));

  size_t tpos = 0;
  int inc = 0;

  tape[0] = (getchar() - (int)'0');

  for (inc = getchar(); inc != EOF; inc = getchar()) {
    if (inc == (int)',') {
      tape[++tpos] = (getchar() - (int)'0');
      continue;
    }

    tape[tpos] *= 10;
    tape[tpos] += (inc - (int)'0');
  }

  int noun = -1;
  int verb = 0;
  int temptape[10000];
  int value = 0;
  memcpy(temptape, tape, sizeof(tape));

  while (value != 19690720) {
    ++noun;
    if (noun > 99) {
      noun = 0;
      ++verb;
    }

    tape[1] = noun;
    tape[2] = verb;
    simulate();
    value = tape[0];
    memcpy(tape, temptape, sizeof(tape));
  }

  printf("%d %d %d", noun, verb, 100*noun + verb);

  showtiming(start);

  return 0;
}
