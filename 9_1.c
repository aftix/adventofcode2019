#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "timing.h"
#include "intcode.h"

int
main(int argc, char ** argv)
{
  uint64_t start = ns();
  FILE * instr = fopen(argv[1], "r");
  memset(maintape, 0, sizeof(maintape));

  size_t tpos = 0;
  int inc = 0;
  int sign = 1;

  inc = getc(instr);
  if (inc == (int)'-') {
    sign = -1;
    maintape[0] = (getc(instr) - (int)'0');
  } else if (inc == (int)'+') {
    maintape[0] = (getc(instr) - (int)'0');
  } else {
    maintape[0] = (inc - (int)'0');
  }

  for (inc = getc(instr); inc != EOF && inc != '\n'; inc = getc(instr)) {
    if (inc == (int)',') {
      if (sign < 0) {
        maintape[tpos] = -maintape[tpos];
      }
      inc = getc(instr);
      if (inc == (int)'-') {
        sign = -1;
        maintape[++tpos] = (getc(instr) - (int)'0');
      } else if (inc == (int)'+') {
        sign = 1;
        maintape[++tpos] = (getc(instr) - (int)'0');
      } else {
        sign = 1;
        maintape[++tpos] = (inc - (int)'0');
      }
      continue;
    }

    maintape[tpos] *= 10;
    maintape[tpos] += (inc - (int)'0');
  }
  fclose(instr);

  intcode_run(maintape, 0, 1, true, true);

  showtiming(start);

  return 0;
}
