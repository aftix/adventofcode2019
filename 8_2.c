#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <limits.h>

#include "timing.h"

const int width = 25;
const int height = 6;
const int maxlayers = 128;

int
main(int argc, char ** argv)
{
  uint64_t start = ns();

  int layers[maxlayers][width * height];
  int layer = 0;
  int lpos = 0;

  for (int inc = getchar(); inc != EOF; inc = getchar()) {
    layers[layer][lpos++] = (inc - (int)'0');

    if (lpos >= width * height) {
      lpos = 0;
      ++layer;
    }
  }

  for (int pos = 0; pos < width * height; ++pos) {
    int lpos = 0;
    for (lpos = 0; lpos < layer; ++lpos) {
      if (layers[lpos][pos] != 2) {
        layers[0][pos] = layers[lpos][pos];
        break;
      }
    }
    if (lpos == layer) {
      layers[0][pos] = 2;
    }
    if (pos && !(pos % width)) {
      putc('\n', stdout);
    }
    putc((char)layers[0][pos] + '0', stdout);
  }
  putc('\n', stdout);


  showtiming(start);
  return 0;
}
