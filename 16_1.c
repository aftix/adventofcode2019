#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <stdint.h>

#include "timing.h"

struct csr {
  size_t dimension;
  size_t nnz;
  int * vals;
  size_t * rowptr;
  size_t * cols;
};

size_t T_n = 0;
size_t sq = 0;


// out <- m*d
void
mat_vec(const struct csr * m, const long * d, long * out)
{
  for (size_t k=0; k < m->dimension; ++k) {
    out[k] = 0;
  }

  for (size_t i=0; i < m->dimension; ++i) {
    for (size_t k = m->rowptr[i]; k < m->rowptr[i + 1]; ++k) {
      out[i] += m->vals[k] * d[m->cols[k]];
    }
  }

  for (size_t i = 0; i < m->dimension; ++i) {
    out[i] = labs(out[i]) % 10;
  }
}

int
main(int argc, char ** argv)
{
  uint64_t start = ns();

  char * input = NULL;
  size_t inlen = 0;

  FILE * buf = open_memstream(&input, &inlen);

  for (int inc = getchar(); inc != EOF && inc != '\n'; inc = getchar()) {
    fputc((char)inc - '0', buf);
  }

  fclose(buf);

  long * vec = malloc(sizeof(long) * inlen);
  for (size_t i=0; i < inlen; ++i) {
    vec[i] = (long)input[i];
  }

  // Maximum elements of upper n x n triangular matrix is the nth triangle number
  T_n = (inlen * (inlen - 1)) >> 1;
  sq = inlen * inlen;

  struct csr phase;
  phase.dimension = inlen;
  phase.nnz = 0;
  phase.vals = malloc(sizeof(*phase.vals) * T_n);
  phase.cols = malloc(sizeof(*phase.cols) * T_n);
  phase.rowptr = malloc(sizeof(*phase.rowptr) * (inlen + 1));

  phase.rowptr[0] = 0;
  size_t prows = 1;

  const int pattern[] = {1, 0, -1, 0};

  for (size_t row=0; row < inlen; ++row) {
    phase.rowptr[prows] = phase.rowptr[prows - 1];

    size_t patternpos = 0;
    size_t incpattern = 0;
    for (size_t col = row; col < inlen; ++col) {
      if (pattern[patternpos]) {
        phase.vals[phase.nnz] = pattern[patternpos];
        phase.cols[phase.nnz++] = col;
        ++phase.rowptr[prows];
      }

      ++incpattern;
      if (incpattern == row + 1) {
        incpattern = 0;
        ++patternpos;
        if (patternpos == sizeof(pattern) / sizeof(pattern[0])) {
          patternpos = 0;
        }
      }
    }
    ++prows;
  }

  long * temp = malloc(sizeof(long) * inlen);

  for (size_t i=0; i < 50; ++i) {
    mat_vec(&phase, vec, temp);
    mat_vec(&phase, temp, vec);
  }

  for (size_t i=0; i < 8; ++i) {
    putc((char)vec[i] + '0', stdout);
  }
  putc('\n', stdout);

  showtiming(start);
}
