#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "timing.h"

int
main(int argc, char ** argv)
{
  uint64_t start = ns();
  FILE * instr = fopen(argv[1], "r");
  int tape[1000];
  memset(tape, 0, sizeof(tape));

  size_t tpos = 0;
  int inc = 0;
  int sign = 1;

  inc = getc(instr);
  if (inc == (int)'-') {
    sign = -1;
    tape[0] = (getc(instr) - (int)'0');
  } else if (inc == (int)'+') {
    tape[0] = (getc(instr) - (int)'0');
  } else {
    tape[0] = (inc - (int)'0');
  }

  for (inc = getc(instr); inc != EOF && inc != '\n'; inc = getc(instr)) {
    if (inc == (int)',') {
      if (sign < 0) {
        tape[tpos] = -tape[tpos];
      }
      inc = getc(instr);
      if (inc == (int)'-') {
        sign = -1;
        tape[++tpos] = (getc(instr) - (int)'0');
      } else if (inc == (int)'+') {
        sign = 1;
        tape[++tpos] = (getc(instr) - (int)'0');
      } else {
        sign = 1;
        tape[++tpos] = (inc - (int)'0');
      }
      continue;
    }

    tape[tpos] *= 10;
    tape[tpos] += (inc - (int)'0');
  }
  fclose(instr);


  tpos = 0;
  int ch = 0;
  int num = 0;

  while (tape[tpos] != 99) {
    switch (tape[tpos]) {
    // Opcode 1: ADD
    case 1:
      tape[tape[tpos + 3]] = tape[tape[tpos + 1]] + tape[tape[tpos + 2]];
      tpos += 4;
      break;
    case 101:
      tape[tape[tpos + 3]] = tape[tpos + 1] + tape[tape[tpos + 2]];
      tpos += 4;
      break;
    case 1001:
      tape[tape[tpos + 3]] = tape[tape[tpos + 1]] + tape[tpos + 2];
      tpos += 4;
      break;
    case 1101:
      tape[tape[tpos + 3]] = tape[tpos + 1] + tape[tpos + 2];
      tpos += 4;
      break;

    //Opcode 2: MULT
    case 2:
      tape[tape[tpos + 3]] = tape[tape[tpos + 1]] * tape[tape[tpos + 2]];
      tpos += 4;
      break;
    case 102:
      tape[tape[tpos + 3]] = tape[tpos + 1] * tape[tape[tpos + 2]];
      tpos += 4;
      break;
    case 1002:
      tape[tape[tpos + 3]] = tape[tape[tpos + 1]] * tape[tpos + 2];
      tpos += 4;
      break;
    case 1102:
      tape[tape[tpos + 3]] = tape[tpos + 1] * tape[tpos + 2];
      tpos += 4;
      break;

    //Opcode 3: RDI
    case 3:
      ch = getchar();
      if (ch == EOF) {
        fprintf(stderr, "Need an input\n");
        return 1;
      } else if (ch == (int)'-') {
        ch = -1;
        num = (getchar() - (int)'0');
      } else if (ch == (int)'+') {
        ch = 1;
        num = (getchar() - (int)'0');
      } else {
        num = (ch - (int)'0');
        ch = 1;
      }

      for (int c = getchar(); c != EOF && c != '\n'; c = getchar()) {
        num *= 10;
        num += (c - (int)'0');
      }
      tape[tape[tpos + 1]] = num * ch;
      tpos += 2;
      break;

    //Opcode 4: PRI
    case 4:
      printf("%d\n", tape[tape[tpos + 1]]);
      tpos += 2;
      break;
    case 104:
      printf("%d\n", tape[tpos + 1]);
      tpos += 2;
      break;

    //Opcode 5: JNZ
    case 5:
      if (tape[tape[tpos + 1]]) {
        tpos = tape[tape[tpos + 2]];
      } else {
        tpos += 3;
      }
      break;
    case 105:
      if (tape[tpos + 1]) {
        tpos = tape[tape[tpos + 2]];
      } else {
        tpos += 3;
      }
      break;
    case 1005:
      if (tape[tape[tpos + 1]]) {
        tpos = tape[tpos + 2];
      } else {
        tpos += 3;
      }
      break;
    case 1105:
      if (tape[tpos + 1]) {
        tpos = tape[tpos + 2];
      } else {
        tpos += 3;
      }
      break;

    // Opcode 6: JZ
    case 6:
      if (!tape[tape[tpos + 1]]) {
        tpos = tape[tape[tpos + 2]];
      } else {
        tpos += 3;
      }
      break;
    case 106:
      if (!tape[tpos + 1]) {
        tpos = tape[tape[tpos + 2]];
      } else {
        tpos += 3;
      }
      break;
    case 1006:
      if (!tape[tape[tpos + 1]]) {
        tpos = tape[tpos + 2];
      } else {
        tpos += 3;
      }
      break;
    case 1106:
      if (!tape[tpos + 1]) {
        tpos = tape[tpos + 2];
      } else {
        tpos += 3;
      }
      break;

    // Opcode 7: LT
    case 7:
      if (tape[tape[tpos + 1]] < tape[tape[tpos + 2]]) {
        tape[tape[tpos + 3]] = 1;
      } else {
        tape[tape[tpos + 3]] = 0;
      }
      tpos += 4;
      break;
    case 107:
      if (tape[tpos + 1] < tape[tape[tpos + 2]]) {
        tape[tape[tpos + 3]] = 1;
      } else {
        tape[tape[tpos + 3]] = 0;
      }
      tpos += 4;
      break;
    case 1007:
      if (tape[tape[tpos + 1]] < tape[tpos + 2]) {
        tape[tape[tpos + 3]] = 1;
      } else {
        tape[tape[tpos + 3]] = 0;
      }
      tpos += 4;
      break;
    case 1107:
      if (tape[tpos + 1] < tape[tpos + 2]) {
        tape[tape[tpos + 3]] = 1;
      } else {
        tape[tape[tpos + 3]] = 0;
      }
      tpos += 4;
      break;

    // Opcode 8: EQ
    case 8:
      if (tape[tape[tpos + 1]] == tape[tape[tpos + 2]]) {
        tape[tape[tpos + 3]] = 1;
      } else {
        tape[tape[tpos + 3]] = 0;
      }
      tpos += 4;
      break;
    case 108:
      if (tape[tpos + 1] == tape[tape[tpos + 2]]) {
        tape[tape[tpos + 3]] = 1;
      } else {
        tape[tape[tpos + 3]] = 0;
      }
      tpos += 4;
      break;
    case 1008:
      if (tape[tape[tpos + 1]] == tape[tpos + 2]) {
        tape[tape[tpos + 3]] = 1;
      } else {
        tape[tape[tpos + 3]] = 0;
      }
      tpos += 4;
      break;
    case 1108:
      if (tape[tpos + 1] == tape[tpos + 2]) {
        tape[tape[tpos + 3]] = 1;
      } else {
        tape[tape[tpos + 3]] = 0;
      }
      tpos += 4;
      break;

    // No opcode
    default:
      fprintf(stderr, "Invalid opcode %d\n", tape[tpos]);
      return 1;
    }
  }

  showtiming(start);

  return 0;
}
