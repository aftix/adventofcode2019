#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <qlibc/qlibc.h>

#include "timing.h"

struct pair {
  int x, y;
};

int
main(int argc, char ** argv)
{
  uint64_t start = ns();

  struct pair pos = {0, 0};

  qvector_t * astroids = qvector(64, sizeof(struct pair), QVECTOR_RESIZE_LINEAR);

  int ch = 0;
  for (ch = getchar(); ch != EOF; ch = getchar()) {
    if (ch == (int)'\n') {
      pos.x = 0;
      ++pos.y;
      continue;
    }

    if (ch == (int)'#') {
      astroids->addlast(astroids, &pos);
    }

    ++pos.x;
  }


  qvector_obj_t lookat;
  memset(&lookat, 0, sizeof(lookat));

  struct pair slope = {0, 0};
  struct pair * pslp = NULL;

  int maxseeing = 0;
  struct pair maxpos = {0, 0};

  while (astroids->getnext(astroids, &lookat, false)) {
    qvector_t * badslopes = qvector(128, sizeof(struct pair), QVECTOR_RESIZE_LINEAR);
    struct pair * proposed = lookat.data;

    qvector_obj_t other;
    memset(&other, 0, sizeof(other));

    int abletosee = 0;

    while (astroids->getnext(astroids, &other, false)) {
      struct pair * cansee = other.data;
      if (proposed == cansee) {
        continue;
      }

      slope.x = proposed->x - cansee->x;
      slope.y = proposed->y - cansee->y;


      bool isgood = true;

      size_t slen = badslopes->size(badslopes);
      for (size_t i=0; i < slen; ++i) {
        pslp = badslopes->getat(badslopes, i, false);
        if (pslp->x == 0 && slope.x == 0) {
          isgood = false;
          break;
        } else if (pslp->x == 0) {
          continue;
        } else if (slope.x == 0) {
          continue;
        } else if (slope.y * pslp->x == slope.x * pslp->y) {
          isgood = false;
          break;
        }
      }

      if (isgood) {
        ++abletosee;
        badslopes->addlast(badslopes, &slope);
        pslp = badslopes->getlast(badslopes, false);
      }
    }

    if (abletosee > maxseeing) {
      maxseeing = abletosee;
      maxpos.x = proposed->x;
      maxpos.y = proposed->y;
    }
  }

  printf("(%d, %d) %d\n", maxpos.x, maxpos.y, maxseeing);

  showtiming(start);
  return 0;
}
