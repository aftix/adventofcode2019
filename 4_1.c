#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

#include "timing.h"

const int starts[] = {
  999999,
  888888,
  777777,
  666666,
  555555,
  444444,
  333333,
  222222,
  111111
};

const int ends[] = {
  1000000,
  900000,
  800000,
  700000,
  600000,
  500000,
  400000,
  300000,
  200000
};

static inline bool
goodcode(int code)
{
  int lastdigit = 10;
  bool repeated = false;

  for (unsigned int i=0; i < 6; ++i) {
    int digit = code % 10;
    code /= 10;
    if (digit > lastdigit) {
      return false;
    } else if (digit == lastdigit) {
      repeated = true;
    }

    lastdigit = digit;
  }

  return repeated;
}


int
main(int argc, char ** argv)
{
  uint64_t start = ns();

  int lower = (getchar() - (int)'0');

  for (unsigned int i=1; i < 6; ++i) {
    lower *= 10;
    lower += (getchar() - (int)'0');
  }

  getchar();

  int higher = (getchar() - (int)'0');

  for (unsigned int i=1; i < 6; ++i) {
    higher *= 10;
    higher += (getchar() - (int)'0');
  }

  int endpos = sizeof(ends) / sizeof(ends[0]) - 1;
  do {
    endpos--;
  } while (starts[endpos] < lower && endpos != -1);
  endpos++;
  if (ends[endpos] < lower) {
    endpos--;
  }

  unsigned int counter = 0;

  for (int i= lower > starts[endpos] ? lower : starts[endpos]; i < ends[endpos] && i <= higher; ++i) {
    if (goodcode(i)) {
      ++counter;
    }
  }
  --endpos;

  if (endpos >= 0 && starts[endpos] > higher) {
    endpos = -1;
  }

  while (endpos >= 0) {
    for (int i=starts[endpos]; i < ends[endpos] && i <= higher; ++i) {
      if (goodcode(i)) {
        ++counter;
      }
    }
    --endpos;

    if (endpos >= 0 && starts[endpos] > higher) {
      endpos = -1;
    }
  }

  printf("%d codes\n", counter);

  showtiming(start);
  return 0;
}
