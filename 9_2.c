#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "timing.h"

int
main(int argc, char ** argv)
{
  uint64_t start = ns();
  FILE * instr = fopen(argv[1], "r");
  long tape[4096];
  memset(tape, 0, sizeof(tape));

  size_t tpos = 0;
  int inc = 0;
  int sign = 1;

  inc = getc(instr);
  if (inc == (int)'-') {
    sign = -1;
    tape[0] = (getc(instr) - (int)'0');
  } else if (inc == (int)'+') {
    tape[0] = (getc(instr) - (int)'0');
  } else {
    tape[0] = (inc - (int)'0');
  }

  for (inc = getc(instr); inc != EOF && inc != '\n'; inc = getc(instr)) {
    if (inc == (int)',') {
      if (sign < 0) {
        tape[tpos] = -tape[tpos];
      }
      inc = getc(instr);
      if (inc == (int)'-') {
        sign = -1;
        tape[++tpos] = (getc(instr) - (int)'0');
      } else if (inc == (int)'+') {
        sign = 1;
        tape[++tpos] = (getc(instr) - (int)'0');
      } else {
        sign = 1;
        tape[++tpos] = (inc - (int)'0');
      }
      continue;
    }

    tape[tpos] *= 10;
    tape[tpos] += (inc - (int)'0');
  }
  fclose(instr);


  tpos = 0;
  int ch = 0;
  long num = 0;
  long offset = 0;

  while (tape[tpos] != 99) {
    switch (tape[tpos]) {
    // Opcode 1: ADD
    case 1:
      tape[tape[tpos + 3]] = tape[tape[tpos + 1]] + tape[tape[tpos + 2]];
      tpos += 4;
      break;
    case 101:
      tape[tape[tpos + 3]] = tape[tpos + 1] + tape[tape[tpos + 2]];
      tpos += 4;
      break;
    case 201:
      tape[tape[tpos + 3]] = tape[tape[tpos + 1] + offset] + tape[tape[tpos + 2]];
      tpos += 4;
      break;
    case 1001:
      tape[tape[tpos + 3]] = tape[tape[tpos + 1]] + tape[tpos + 2];
      tpos += 4;
      break;
    case 1101:
      tape[tape[tpos + 3]] = tape[tpos + 1] + tape[tpos + 2];
      tpos += 4;
      break;
    case 1201:
      tape[tape[tpos + 3]] = tape[tape[tpos + 1] + offset] + tape[tpos + 2];
      tpos += 4;
      break;
    case 2001:
      tape[tape[tpos + 3]] = tape[tape[tpos + 1] + offset] + tape[tape[tpos + 2]];
      tpos += 4;
      break;
    case 2101:
      tape[tape[tpos + 3]] = tape[tpos + 1] + tape[tape[tpos + 2] + offset];
      tpos += 4;
      break;
    case 2201:
      tape[tape[tpos + 3]] = tape[tape[tpos + 1] + offset] + tape[tape[tpos + 2] + offset];
      tpos += 4;
      break;
    case 20001:
      tape[tape[tpos + 3] + offset] = tape[tape[tpos + 1]] + tape[tape[tpos + 2]];
      tpos += 4;
      break;
    case 20101:
      tape[tape[tpos + 3] + offset] = tape[tpos + 1] + tape[tape[tpos + 2]];
      tpos += 4;
      break;
    case 20201:
      tape[tape[tpos + 3] + offset] = tape[tape[tpos + 1] + offset] + tape[tape[tpos + 2]];
      tpos += 4;
      break;
    case 21001:
      tape[tape[tpos + 3] + offset] = tape[tape[tpos + 1]] + tape[tpos + 2];
      tpos += 4;
      break;
    case 21101:
      tape[tape[tpos + 3] + offset] = tape[tpos + 1] + tape[tpos + 2];
      tpos += 4;
      break;
    case 21201:
      tape[tape[tpos + 3] + offset] = tape[tape[tpos + 1] + offset] + tape[tpos + 2];
      tpos += 4;
      break;
    case 22001:
      tape[tape[tpos + 3] + offset] = tape[tape[tpos + 1] + offset] + tape[tape[tpos + 2]];
      tpos += 4;
      break;
    case 22101:
      tape[tape[tpos + 3] + offset] = tape[tpos + 1] + tape[tape[tpos + 2] + offset];
      tpos += 4;
      break;
    case 22201:
      tape[tape[tpos + 3] + offset] = tape[tape[tpos + 1] + offset] + tape[tape[tpos + 2] + offset];
      tpos += 4;
      break;

    //Opcode 2: MULT
    case 2:
      tape[tape[tpos + 3]] = tape[tape[tpos + 1]] * tape[tape[tpos + 2]];
      tpos += 4;
      break;
    case 102:
      tape[tape[tpos + 3]] = tape[tpos + 1] * tape[tape[tpos + 2]];
      tpos += 4;
      break;
    case 202:
      tape[tape[tpos + 3]] = tape[tape[tpos + 1] + offset] * tape[tape[tpos + 2]];
      tpos += 4;
      break;
    case 1002:
      tape[tape[tpos + 3]] = tape[tape[tpos + 1]] * tape[tpos + 2];
      tpos += 4;
      break;
    case 1102:
      tape[tape[tpos + 3]] = tape[tpos + 1] * tape[tpos + 2];
      tpos += 4;
      break;
    case 1202:
      tape[tape[tpos + 3]] = tape[tape[tpos + 1] + offset] * tape[tpos + 2];
      tpos += 4;
      break;
    case 2002:
      tape[tape[tpos + 3]] = tape[tape[tpos + 1]] * tape[tape[tpos + 2] + offset];
      tpos += 4;
      break;
    case 2102:
      tape[tape[tpos + 3]] = tape[tpos + 1] * tape[tape[tpos + 2] + offset];
      tpos += 4;
      break;
    case 2202:
      tape[tape[tpos + 3]] = tape[tape[tpos + 1] + offset] * tape[tape[tpos + 2] + offset];
      tpos += 4;
      break;
    case 20002:
      tape[tape[tpos + 3] + offset] = tape[tape[tpos + 1]] * tape[tape[tpos + 2]];
      tpos += 4;
      break;
    case 20102:
      tape[tape[tpos + 3] + offset] = tape[tpos + 1] * tape[tape[tpos + 2]];
      tpos += 4;
      break;
    case 20202:
      tape[tape[tpos + 3] + offset] = tape[tape[tpos + 1] + offset] * tape[tape[tpos + 2]];
      tpos += 4;
      break;
    case 21002:
      tape[tape[tpos + 3] + offset] = tape[tape[tpos + 1]] * tape[tpos + 2];
      tpos += 4;
      break;
    case 21102:
      tape[tape[tpos + 3] + offset] = tape[tpos + 1] * tape[tpos + 2];
      tpos += 4;
      break;
    case 21202:
      tape[tape[tpos + 3] + offset] = tape[tape[tpos + 1] + offset] * tape[tpos + 2];
      tpos += 4;
      break;
    case 22002:
      tape[tape[tpos + 3] + offset] = tape[tape[tpos + 1]] * tape[tape[tpos + 2] + offset];
      tpos += 4;
      break;
    case 22102:
      tape[tape[tpos + 3] + offset] = tape[tpos + 1] * tape[tape[tpos + 2] + offset];
      tpos += 4;
      break;
    case 22202:
      tape[tape[tpos + 3] + offset] = tape[tape[tpos + 1] + offset] * tape[tape[tpos + 2] + offset];
      tpos += 4;
      break;

    //Opcode 3: RDI
    case 3:
      ch = getchar();
      if (ch == EOF) {
        fprintf(stderr, "Need an input\n");
        return 1;
      } else if (ch == (int)'-') {
        ch = -1;
        num = (getchar() - (int)'0');
      } else if (ch == (int)'+') {
        ch = 1;
        num = (getchar() - (int)'0');
      } else {
        num = (ch - (int)'0');
        ch = 1;
      }

      for (int c = getchar(); c != EOF && c != '\n'; c = getchar()) {
        num *= 10;
        num += (c - (int)'0');
      }
      tape[tape[tpos + 1]] = num * ch;
      tpos += 2;
      break;
    case 203:
      ch = getchar();
      if (ch == EOF) {
        fprintf(stderr, "Need an input\n");
        return 1;
      } else if (ch == (int)'-') {
        ch = -1;
        num = (getchar() - (int)'0');
      } else if (ch == (int)'+') {
        ch = 1;
        num = (getchar() - (int)'0');
      } else {
        num = (ch - (int)'0');
        ch = 1;
      }

      for (int c = getchar(); c != EOF && c != '\n'; c = getchar()) {
        num *= 10;
        num += (c - (int)'0');
      }
      tape[tape[tpos + 1] + offset] = num * ch;
      tpos += 2;
      break;

    //Opcode 4: PRI
    case 4:
      printf("%ld\n", tape[tape[tpos + 1]]);
      tpos += 2;
      break;
    case 104:
      printf("%ld\n", tape[tpos + 1]);
      tpos += 2;
      break;
    case 204:
      printf("%ld\n", tape[tape[tpos + 1] + offset]);
      tpos += 2;
      break;

    //Opcode 5: JNZ
    case 5:
      if (tape[tape[tpos + 1]]) {
        tpos = tape[tape[tpos + 2]];
      } else {
        tpos += 3;
      }
      break;
    case 105:
      if (tape[tpos + 1]) {
        tpos = tape[tape[tpos + 2]];
      } else {
        tpos += 3;
      }
      break;
    case 205:
      if (tape[tape[tpos + 1] + offset]) {
        tpos = tape[tape[tpos + 2]];
      } else {
        tpos += 3;
      }
      break;
    case 1005:
      if (tape[tape[tpos + 1]]) {
        tpos = tape[tpos + 2];
      } else {
        tpos += 3;
      }
      break;
    case 1105:
      if (tape[tpos + 1]) {
        tpos = tape[tpos + 2];
      } else {
        tpos += 3;
      }
      break;
    case 1205:
      if (tape[tape[tpos + 1] + offset]) {
        tpos = tape[tpos + 2];
      } else {
        tpos += 3;
      }
      break;
    case 2005:
      if (tape[tape[tpos + 1]]) {
        tpos = tape[tape[tpos + 2] + offset];
      } else {
        tpos += 3;
      }
      break;
    case 2105:
      if (tape[tpos + 1]) {
        tpos = tape[tape[tpos + 2] + offset];
      } else {
        tpos += 3;
      }
      break;
    case 2205:
      if (tape[offset + tape[tpos + 1]]) {
        tpos = tape[tape[tpos + 2] + offset];
      } else {
        tpos += 3;
      }
      break;

    // Opcode 6: JZ
    case 6:
      if (!tape[tape[tpos + 1]]) {
        tpos = tape[tape[tpos + 2]];
      } else {
        tpos += 3;
      }
      break;
    case 106:
      if (!tape[tpos + 1]) {
        tpos = tape[tape[tpos + 2]];
      } else {
        tpos += 3;
      }
      break;
    case 206:
      if (!tape[tape[tpos + 1] + offset]) {
        tpos = tape[tape[tpos + 2]];
      } else {
        tpos += 3;
      }
      break;
    case 1006:
      if (!tape[tape[tpos + 1]]) {
        tpos = tape[tpos + 2];
      } else {
        tpos += 3;
      }
      break;
    case 1106:
      if (!tape[tpos + 1]) {
        tpos = tape[tpos + 2];
      } else {
        tpos += 3;
      }
      break;
    case 1206:
      if (!tape[tape[tpos + 1] + offset]) {
        tpos = tape[tpos + 2];
      } else {
        tpos += 3;
      }
      break;
    case 2006:
      if (!tape[tape[tpos + 1]]) {
        tpos = tape[tape[tpos + 2] + offset];
      } else {
        tpos += 3;
      }
      break;
    case 2106:
      if (!tape[tpos + 1]) {
        tpos = tape[tape[tpos + 2] + offset];
      } else {
        tpos += 3;
      }
      break;
    case 2206:
      if (!tape[tape[tpos + 1] + offset]) {
        tpos = tape[tape[tpos + 2] + offset];
      } else {
        tpos += 3;
      }
      break;

    // Opcode 7: LT
    case 7:
      if (tape[tape[tpos + 1]] < tape[tape[tpos + 2]]) {
        tape[tape[tpos + 3]] = 1;
      } else {
        tape[tape[tpos + 3]] = 0;
      }
      tpos += 4;
      break;
    case 107:
      if (tape[tpos + 1] < tape[tape[tpos + 2]]) {
        tape[tape[tpos + 3]] = 1;
      } else {
        tape[tape[tpos + 3]] = 0;
      }
      tpos += 4;
      break;
    case 207:
      if (tape[tape[tpos + 1] + offset] < tape[tape[tpos + 2]]) {
        tape[tape[tpos + 3]] = 1;
      } else {
        tape[tape[tpos + 3]] = 0;
      }
      tpos += 4;
      break;
    case 1007:
      if (tape[tape[tpos + 1]] < tape[tpos + 2]) {
        tape[tape[tpos + 3]] = 1;
      } else {
        tape[tape[tpos + 3]] = 0;
      }
      tpos += 4;
      break;
    case 1107:
      if (tape[tpos + 1] < tape[tpos + 2]) {
        tape[tape[tpos + 3]] = 1;
      } else {
        tape[tape[tpos + 3]] = 0;
      }
      tpos += 4;
      break;
    case 1207:
      if (tape[tape[tpos + 1] + offset] < tape[tpos + 2]) {
        tape[tape[tpos + 3]] = 1;
      } else {
        tape[tape[tpos + 3]] = 0;
      }
      tpos += 4;
      break;
    case 2007:
      if (tape[tape[tpos + 1]] < tape[tape[tpos + 2] + offset]) {
        tape[tape[tpos + 3]] = 1;
      } else {
        tape[tape[tpos + 3]] = 0;
      }
      tpos += 4;
      break;
    case 2107:
      if (tape[tpos + 1] < tape[tape[tpos + 2] + offset]) {
        tape[tape[tpos + 3]] = 1;
      } else {
        tape[tape[tpos + 3]] = 0;
      }
      tpos += 4;
      break;
    case 2207:
      if (tape[offset + tape[tpos + 1]] < tape[tape[tpos + 2] + offset]) {
        tape[tape[tpos + 3]] = 1;
      } else {
        tape[tape[tpos + 3]] = 0;
      }
      tpos += 4;
      break;
    case 20007:
      if (tape[tape[tpos + 1]] < tape[tape[tpos + 2]]) {
        tape[tape[tpos + 3] + offset] = 1;
      } else {
        tape[tape[tpos + 3] + offset] = 0;
      }
      tpos += 4;
      break;
    case 20107:
      if (tape[tpos + 1] < tape[tape[tpos + 2]]) {
        tape[tape[tpos + 3] + offset] = 1;
      } else {
        tape[tape[tpos + 3] + offset] = 0;
      }
      tpos += 4;
      break;
    case 20207:
      if (tape[tape[tpos + 1] + offset] < tape[tape[tpos + 2]]) {
        tape[tape[tpos + 3] + offset] = 1;
      } else {
        tape[tape[tpos + 3] + offset] = 0;
      }
      tpos += 4;
      break;
    case 21007:
      if (tape[tape[tpos + 1]] < tape[tpos + 2]) {
        tape[tape[tpos + 3] + offset] = 1;
      } else {
        tape[tape[tpos + 3] + offset] = 0;
      }
      tpos += 4;
      break;
    case 21107:
      if (tape[tpos + 1] < tape[tpos + 2]) {
        tape[tape[tpos + 3] + offset] = 1;
      } else {
        tape[tape[tpos + 3] + offset] = 0;
      }
      tpos += 4;
      break;
    case 21207:
      if (tape[tape[tpos + 1] + offset] < tape[tpos + 2]) {
        tape[tape[tpos + 3] + offset] = 1;
      } else {
        tape[tape[tpos + 3] + offset] = 0;
      }
      tpos += 4;
      break;
    case 22007:
      if (tape[tape[tpos + 1]] < tape[tape[tpos + 2] + offset]) {
        tape[tape[tpos + 3] + offset] = 1;
      } else {
        tape[tape[tpos + 3] + offset] = 0;
      }
      tpos += 4;
      break;
    case 22107:
      if (tape[tpos + 1] < tape[tape[tpos + 2] + offset]) {
        tape[tape[tpos + 3] + offset] = 1;
      } else {
        tape[tape[tpos + 3] + offset] = 0;
      }
      tpos += 4;
      break;
    case 22207:
      if (tape[offset + tape[tpos + 1]] < tape[tape[tpos + 2] + offset]) {
        tape[tape[tpos + 3] + offset] = 1;
      } else {
        tape[tape[tpos + 3] + offset] = 0;
      }
      tpos += 4;
      break;

    // Opcode 8: EQ
    case 8:
      if (tape[tape[tpos + 1]] == tape[tape[tpos + 2]]) {
        tape[tape[tpos + 3]] = 1;
      } else {
        tape[tape[tpos + 3]] = 0;
      }
      tpos += 4;
      break;
    case 108:
      if (tape[tpos + 1] == tape[tape[tpos + 2]]) {
        tape[tape[tpos + 3]] = 1;
      } else {
        tape[tape[tpos + 3]] = 0;
      }
      tpos += 4;
      break;
    case 208:
      if (tape[tape[tpos + 1] + offset] == tape[tape[tpos + 2]]) {
        tape[tape[tpos + 3]] = 1;
      } else {
        tape[tape[tpos + 3]] = 0;
      }
      tpos += 4;
      break;
    case 1008:
      if (tape[tape[tpos + 1]] == tape[tpos + 2]) {
        tape[tape[tpos + 3]] = 1;
      } else {
        tape[tape[tpos + 3]] = 0;
      }
      tpos += 4;
      break;
    case 1108:
      if (tape[tpos + 1] == tape[tpos + 2]) {
        tape[tape[tpos + 3]] = 1;
      } else {
        tape[tape[tpos + 3]] = 0;
      }
      tpos += 4;
      break;
    case 1208:
      if (tape[offset + tape[tpos + 1]] == tape[tpos + 2]) {
        tape[tape[tpos + 3]] = 1;
      } else {
        tape[tape[tpos + 3]] = 0;
      }
      tpos += 4;
      break;
    case 2008:
      if (tape[tape[tpos + 1]] == tape[tape[tpos + 2] + offset]) {
        tape[tape[tpos + 3]] = 1;
      } else {
        tape[tape[tpos + 3]] = 0;
      }
      tpos += 4;
      break;
    case 2108:
      if (tape[tpos + 1] == tape[tape[tpos + 2] + offset]) {
        tape[tape[tpos + 3]] = 1;
      } else {
        tape[tape[tpos + 3]] = 0;
      }
      tpos += 4;
      break;
    case 2208:
      if (tape[tape[tpos + 1] + offset] == tape[tape[tpos + 2] + offset]) {
        tape[tape[tpos + 3]] = 1;
      } else {
        tape[tape[tpos + 3]] = 0;
      }
      tpos += 4;
      break;
    case 20008:
      if (tape[tape[tpos + 1]] == tape[tape[tpos + 2]]) {
        tape[tape[tpos + 3] + offset] = 1;
      } else {
        tape[tape[tpos + 3] + offset] = 0;
      }
      tpos += 4;
      break;
    case 20108:
      if (tape[tpos + 1] == tape[tape[tpos + 2]]) {
        tape[tape[tpos + 3] + offset] = 1;
      } else {
        tape[tape[tpos + 3] + offset] = 0;
      }
      tpos += 4;
      break;
    case 20208:
      if (tape[tape[tpos + 1] + offset] == tape[tape[tpos + 2]]) {
        tape[tape[tpos + 3] + offset] = 1;
      } else {
        tape[tape[tpos + 3] + offset] = 0;
      }
      tpos += 4;
      break;
    case 21008:
      if (tape[tape[tpos + 1]] == tape[tpos + 2]) {
        tape[tape[tpos + 3] + offset] = 1;
      } else {
        tape[tape[tpos + 3] + offset] = 0;
      }
      tpos += 4;
      break;
    case 21108:
      if (tape[tpos + 1] == tape[tpos + 2]) {
        tape[tape[tpos + 3] + offset] = 1;
      } else {
        tape[tape[tpos + 3] + offset] = 0;
      }
      tpos += 4;
      break;
    case 21208:
      if (tape[offset + tape[tpos + 1]] == tape[tpos + 2]) {
        tape[tape[tpos + 3] + offset] = 1;
      } else {
        tape[tape[tpos + 3] + offset] = 0;
      }
      tpos += 4;
      break;
    case 22008:
      if (tape[tape[tpos + 1]] == tape[tape[tpos + 2] + offset]) {
        tape[tape[tpos + 3] + offset] = 1;
      } else {
        tape[tape[tpos + 3] + offset] = 0;
      }
      tpos += 4;
      break;
    case 22108:
      if (tape[tpos + 1] == tape[tape[tpos + 2] + offset]) {
        tape[tape[tpos + 3] + offset] = 1;
      } else {
        tape[tape[tpos + 3] + offset] = 0;
      }
      tpos += 4;
      break;
    case 22208:
      if (tape[tape[tpos + 1] + offset] == tape[tape[tpos + 2] + offset]) {
        tape[tape[tpos + 3] + offset] = 1;
      } else {
        tape[tape[tpos + 3] + offset] = 0;
      }
      tpos += 4;
      break;

    // Opcode 9: REL
    case 9:
      offset += tape[tape[tpos + 1]];
      tpos += 2;
      break;
    case 109:
      offset += tape[tpos + 1];
      tpos += 2;
      break;
    case 209:
      offset += tape[tape[tpos + 1] + offset];
      tpos += 2;
      break;

    // No opcode
    default:
      fprintf(stderr, "Invalid opcode %ld\n", tape[tpos]);
      return 1;
    }
  }

  showtiming(start);

  return 0;
}
