#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <qlibc/qlibc.h>

#include "timing.h"

struct orbit {
  struct orbit * parent;
  struct orbit * suborbits[64];
  size_t numorbits;
  char name[4];
  int distance;
};

static bool
wavepropigate(struct orbit * src, struct orbit * dest, int depth)
{
  if (!dest) {
    return false;
  }

  if (src == dest) {
    src->distance = depth;
    return true;
  }

  dest->distance = depth;
  for (unsigned int i=0; i < dest->numorbits; ++i) {
    if (dest->suborbits[i]->distance == -1) {
      if (wavepropigate(src, dest->suborbits[i], depth + 1)) {
        return true;
      }
    }
  }

  if (dest->parent && dest->parent->distance == -1) {
    return wavepropigate(src, dest->parent, depth + 1);
  }

  return false;
}

int
main(int argc, char ** argv)
{
  uint64_t start = ns();


  qtreetbl_t * allorbs = qtreetbl(0);

  struct orbit porbit;
  struct orbit corbit;
  struct orbit * pptr = NULL;
  struct orbit * cptr = NULL;

  for (size_t i=0; i < sizeof(corbit.suborbits) / sizeof(corbit.suborbits[0]); ++i) {
    porbit.suborbits[i] = NULL;
    corbit.suborbits[i] = NULL;
  }
  porbit.numorbits = 0;
  corbit.numorbits = 0;
  porbit.distance = -1;
  corbit.distance = -1;
  porbit.parent = NULL;
  corbit.parent = NULL;

  int ch = 0;
  size_t ppos = 0;
  size_t cpos = 0;

  struct orbit ** optr = NULL;

  for (ch = getchar(); ch != EOF; ch = getchar()) {
    while (ch != (int)')') {
      porbit.name[ppos++] = (char)ch;
      ch = getchar();
    }
    porbit.name[ppos] = '\0';
    ppos = 0;

    ch = getchar();
    while (ch != (int)'\n' && ch != EOF) {
      corbit.name[cpos++] = (char)ch;
      ch = getchar();
    }
    corbit.name[cpos] = '\0';
    cpos = 0;

    optr = allorbs->get(allorbs, porbit.name, NULL, false);
    if (!optr) {
      pptr = malloc(sizeof(porbit));
      memcpy(pptr, &porbit, sizeof(porbit));
      allorbs->put(allorbs, porbit.name, &pptr, sizeof(pptr));
    } else {
      pptr = *optr;
    }


    optr = allorbs->get(allorbs, corbit.name, NULL, false);
    if (!optr) {
      cptr = malloc(sizeof(corbit));
      memcpy(cptr, &corbit, sizeof(corbit));
      allorbs->put(allorbs, corbit.name, &cptr, sizeof(cptr));
    } else {
      cptr = *optr;
    }

    pptr->suborbits[pptr->numorbits] = cptr;
    pptr->numorbits += 1;
    cptr->parent = pptr;
  }

  struct orbit * san = *(struct orbit **)allorbs->get(allorbs, "SAN", NULL, false);
  struct orbit * you = *(struct orbit **)allorbs->get(allorbs, "YOU", NULL, false);

  wavepropigate(you, san, 0);

  printf("%d\n", you->distance - 2);

  showtiming(start);
  return 0;
}
