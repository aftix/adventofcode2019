#include <stdio.h>
#include <stdlib.h>

#include "timing.h"

int
main(int argc, char ** argv)
{
  uint64_t start = ns();
  int inc = EOF;
  int sum = 0;
  int in = 0;

  for (inc = getchar(); inc != EOF; inc = getchar()) {
    if (inc == (int)'\n') {
      int newfuel = (in / 3 - 2);
      in = 0;

      do {
        sum += newfuel;
        newfuel /= 3;
        newfuel -= 2;
      } while (newfuel > 0);

      continue;
    }

    if (in) {
      in *= 10;
    }
    in += (inc - (int)'0');
  }

  if (in) {
    int newfuel = (in / 3 - 2);
    do {
      sum += newfuel;
      newfuel /= 3;
      newfuel -= 2;
    } while (newfuel > 0);
  }

  printf("%d\n", sum);
  showtiming(start);
}
