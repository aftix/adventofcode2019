#ifndef INTCODE_H
#define INTCODE_H

#define TAPELEN 8192

#include <stdlib.h>
#include <stdbool.h>

extern long maintape[TAPELEN];

struct intcode_state {
  long * tape;
  int fin;
  int fout;
  size_t tpos;
  int offset;
  bool bufferin;
  bool bufferout;
};

// Run the entire program located on tape until halting
// fin and fout are filedesc for reading in and out
// Bools decide if the input/output are buffered
//
// If input is buffered, read from the fin as if it is stdin (longs in string format)
// else, read as a direct long stream
//
// If output is buffered, write as if to stdout (made into a Cstring with a trailing newline)
// else, read sizeof(long) from fin per input
void intcode_run(long * tape, int fin, int fout, bool bin, bool bout);

// Run a single step, returning true on halt
bool intcode_step(struct intcode_state * state);

#endif //INTCODE_H
